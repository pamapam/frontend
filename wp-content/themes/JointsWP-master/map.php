<?php
$base_dir = dirname(dirname(__FILE__));
require_once($base_dir."../../../wp-load.php");

function getEmbeddedMapConfig($domain, $apiKey) {
	global $baseApiInternalUrl;
	$encodedDomain = str_replace(".", "_", $domain);
	$request = $baseApiInternalUrl . "/embeddedMapConfig/" . $encodedDomain . "/" . $apiKey;
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}

$refererDomain = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
$localDomain = parse_url(get_site_url(), PHP_URL_HOST);
$requestApiKey = $_GET['apiKey'];

if ($refererDomain != $localDomain && $requestApiKey == null) {
	$pamapamSite = true;
	$mapEntityStatuses = array('PUBLISHED');
	$mapFilterTags = array();
	
	include 'embeddedMap.php';
} else {
	$pamapamSite = false;
	$requestApiKey = $_GET['apiKey'];
	$embeddedMapConfig = getEmbeddedMapConfig($refererDomain, $requestApiKey);

 	if ($embeddedMapConfig->status == 200) {
		include 'embeddedMap.php';
 	}
	
}

?>