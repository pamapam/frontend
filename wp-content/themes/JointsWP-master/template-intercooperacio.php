<?php
/*
Template Name: Intercooperació  (No Sidebar)
*/
?>

<?php get_header(); ?>
  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="col-xs-12 col-md-12">
              <div class="box-head">
                <div class="col-xs-12 col-md-12">
                 <h1 class="page-title"><?php the_title(); ?></h1>

                        <?php echo addVesAlMapa(); ?>
                      </div>
              </div>
            </div>

         <?php endif; ?>
      <div class="container">
        <div class="row">
          <main id="main" class="col-xs-12 col-md-12" role="main">
              <?php the_content(); ?>
          </main>
        </div>
      </div>


              <iframe
                      id="map-iframe"
                      width="100%"
                      height="600"
                      frameborder="0" style="border:0" scrolling="no"
                      src="https://pamapam.cat/map/embed?center=[41.7,1.04]&zoom=8&apiKey=y2umvLujD62NAL6nkvRwNmMrdNA5zb0e"
                      allowfullscreen>
              </iframe>

		<!---- Notícies Intercooop ---->
		<div class="container">
			<div class="row">
	    <main id="main" class="col-xs-12 col-md-12 blog" role="main">
	    	<div class="title">
	    		<h4>Notícies Intercooperació</h4>
	    	</div>
	    	<div class="noticies-intercoop row">
            <?php query_posts( array( 'category__and' => array(504), 'posts_per_page' => 3) );?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="col-xs-12 col-lg-4">
                		<?php get_template_part( 'parts/loop', 'archive' ); ?>
                	</div>
				<?php endwhile; ?>
		    <?php endif; wp_reset_query(); ?>
		  </div>



      	<!-- enllaç a veure més noticies -->
      
					<nav class="page-navigation row">
            <div class="col-xs-12 col-md-12">
          	 <a class="button" href="https://pamapam.cat/ca/blog/">Veure més notícies</a>
            </div>
					</nav>

				</main> <!-- end #main -->
    	</div>
    </div>


    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->


<?php get_footer(); ?>


