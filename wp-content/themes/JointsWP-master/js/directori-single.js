function updateDotdotdot() {
	$('.ellipsis').dotdotdot({
		ellipsis : '...',
		wrap : 'word'
	});
}


jQuery(document).ready(function() {
	updateDotdotdot();
	$("section.sectors > p").dotdotdot({
		height : 65,
		fallbackToLetter : true,
		watch : true
	});

});

function getBalanceContents(feature) {


/*
	else if (feature.properties.xesBalance && feature.properties.status === "EXTERNAL"){
		var xesUrl = feature.properties.xesBalanceUrl ? feature.properties.xesBalanceUrl : "balanc-social";
		var balanceText = feature.properties.xesBalanceUrl ? "Aquesta iniciativa ha fet el Balanç Social. Consulta'l" : "Aquesta iniciativa ha fet el Balanç Social.";
		visitaperf = "<a class=\"balanc\" href="+xesUrl+" target=\"_blank\">" +
			"<p>"+balanceText+"</p>" +
			"</a>";
	}
*/
/*
	else if (feature.properties.isIntercoop) {
		var articleIntercoopUrl = '/directori-intercoop/' + feature.properties.normalizedName;

		visitaperf ='<a href="' + articleIntercoopUrl + '" target="_blank" >' +
			'<button id="view-details" type="button" class="control control-text button"><span class="icon-xinxeta" style="padding: 5px;"></span>VISITAR PERFIL</button>' +
			'</a>';
	} else {
        visitaperf = '<a href="' + articleUrl + '" target="_blank" >' +
			""+
            "<button id=\"view-details\" type=\"button\" class=\"control control-text button\"><span class=\"icon-xinxeta padding-right-10\" style=\"padding: 5px;\"></span>VISITAR PERFIL</button>" +
            "</a>";
	}
*/

	if (feature.properties.xesBalance && feature.properties.status !== "EXTERNAL") {

		var xesUrlLogo = feature.properties.xesBalanceUrl ? feature.properties.xesBalanceUrl : "balanc-social";
		visitaperf +=
				'<a href="' + articleUrl + '" target="_blank">' +
					'<img width="40px" class"xes-bs-logo-center" src="/wp-content/themes/JointsWP-master/assets/images/xinxeta1.png\">' +
				'</a>' +
			"<a href="+xesUrlLogo+" target=\"_blank\">" +
			"<img class='xes-bs-logo-center' width='40px' src=" +
			"\""+window.location.protocol +"//"+window.location.hostname+"/wp-content/themes/JointsWP-master/assets/images/b-social.png\"></a>";
	}

	var apiImgPath =  jsBaseResourceUrl + feature.properties.pictureUrl;
	var defaultImgPath = window.location.protocol+"//"+window.location.hostname+"/wp-content/themes/JointsWP-master/assets/images/default_PAP.jpg";
	var img = feature.properties.pictureUrl !== undefined ? apiImgPath : defaultImgPath;

	var contents =
		'<div id="punts" class="punts ficha-punts">' +
			'<div class="entity-slider-box-header">' +
				'<a href="' + articleUrl + '" target="_blank">' +
				'<img src="' + img+'">'+
				'</a>' +
			'</div>' +
		'<h3>' +
		'<a class="titular-ficha" href="' + articleUrl + '" target="_blank">' +
			(feature.properties.name ? feature.properties.name : '') +
		'</a>' +
		'</h3>' +
			'<div class="entity-slider-box-sectors">' +
        '<div class="entity-slider-box-icons">' +
        '<a href="' + articleUrl + '" target="_blank">' +
          sector +
        '</a>' +
//          '&nbsp;<img src=\"' + jsBaseResourceUrl + feature.properties.sectorIconUrls[0] + '\"/>' +
        '</div>' +
      '</div>' +
			'<div class="entity-slider-box-contents">' +
				'<div class="entity-slider-box-description">' +
					'<a href="' + articleUrl + '" target="_blank">' +
					(feature.properties.description ? feature.properties.description : '') +
					'</a>' +
				'</div>' +
//				'<div class="entity-slider-box-criteria">' +
//					'<h4>Criteris</h4>' +
//					criteriaIconsDiv +
//				'</div>' +
				'<div class="entity-slider-box-button">' +
        			visitaperf +
				'</div>' +
			'</div>' +
		'</div>';

	return contents;
}
