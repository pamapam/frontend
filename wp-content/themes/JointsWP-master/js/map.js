var entitesMap = new Map();

function arrayToMap (arr) {
	var entitesMap = new Map();
	if (arr && arr.length > 0) {
		for (let i = 0; i < arr.length; i++) {
			const foo = arr[i]['properties']['id'];
			const value = arr[i];
			entitesMap.set(foo, value);
		}
	}
	return entitesMap;
};


function updateDotdotdot() {
	jQuery(".entity-slider-box-description").dotdotdot({
		ellipsis : '...',
		wrap : 'word',
		height : 70,
		fallbackToLetter : true,
		watch : true
	});

	jQuery(".web").dotdotdot({
		ellipsis : '...',
		wrap : 'word',
		height : 30,
		fallbackToLetter : true,
		watch : true
	});

	if (!jQuery(".web").hasClass("is-truncated")) {
		jQuery(".web a").removeAttr("title");
	}

	jQuery(".adress").dotdotdot({
		ellipsis : '...',
		wrap : 'word',
		height : 20,
		fallbackToLetter : true,
		watch : true
	});

	if (!jQuery(".adress").hasClass("is-truncated")) {
		jQuery(".adress").removeAttr("title");
	}

}


function getLeafletIcon(iconUrl) {
	var leafletIcon = null;
	if (iconUrl == null) {
		leafletIcon = noSectorIcon;
	} else {
		leafletIcon = leafletIcons[iconUrl];
		if (leafletIcon == null) {
			leafletIcon = new L.icon({
            	iconUrl: iconUrl,
            	iconSize: [34, 47],
            	iconAnchor: [17, 46],
				popupAnchor: [0, -40]
        	});
        	leafletIcons[iconUrl] = leafletIcon;
		}
	}

	return leafletIcon;
}

function getSidebarContents(feature) {
  let criteriaIconsDiv = '<div class="entity-slider-box-icons">';
  feature.properties.criteriaIconUrls.forEach((iconUrl) => {
    criteriaIconsDiv += `<img src="${jsBaseResourceUrl}${iconUrl}"/>`;
  });
  criteriaIconsDiv += '</div>';

  let sector = feature.properties.sector;
  const sectores = sector.split(' - ');

  if (sectores.length === 2) {
    sector = sectores[1]; // Si tiene subsector cogerlo
  }

  const articleUrl = `/directori/${feature.properties.normalizedName}`;

  const isExternal = feature.properties.status === 'EXTERNAL';
  const hasXesBalance = feature.properties.xesBalance;

	const greenCommerceBadge = hasGreenCommerceScope = feature.properties.greenCommerceScope ?
		`<a href="/comerc-verd" target="_blank">` +
		`<img class="xes-bs-logo-center" width="40px" src="/wp-content/themes/JointsWP-master/assets/images/green-commerce-badge.png"></a>`
	: '';
  const visitaperf =
    (!hasXesBalance && isExternal && !feature.properties.isIntercoop)
      ? '<p>Aquesta iniciativa no està a Pam a Pam</p>'
      : (hasXesBalance && isExternal)
        ? `<a href="${feature.properties.xesBalanceUrl ? feature.properties.xesBalanceUrl : 'balanc-social'}" target="_blank">` +
            `<img class="xes-bs-logo-center" width="40px" src="/wp-content/themes/JointsWP-master/assets/images/b-social.png"></a>`
        : (hasXesBalance && !isExternal)
          ? `<a href="${feature.properties.xesBalanceUrl ? feature.properties.xesBalanceUrl : 'balanc-social'}" target="_blank">` +
            `<img class="xes-bs-logo-center" width="40px" src="/wp-content/themes/JointsWP-master/assets/images/xinxeta1.png"></a>` +
            `<a href="${feature.properties.xesBalanceUrl ? feature.properties.xesBalanceUrl : 'balanc-social'}" target="_blank">` +
            `<img class="xes-bs-logo-center" width="40px" src="/wp-content/themes/JointsWP-master/assets/images/b-social.png"></a>`
        : (!hasXesBalance && !isExternal)
          ? `<a href="${feature.properties.xesBalanceUrl ? feature.properties.xesBalanceUrl : 'balanc-social'}" target="_blank">` +
            `<img class="xes-bs-logo-center" width="40px" src="/wp-content/themes/JointsWP-master/assets/images/xinxeta1.png"></a>`
          : '';

  const apiImgPath = `${jsBaseResourceUrl}${feature.properties.pictureUrl}`;
  const defaultImgPath = `${window.location.protocol}//${window.location.hostname}/wp-content/themes/JointsWP-master/assets/images/default_PAP.jpg`;
	const greenCommerceImgPath = "/wp-content/themes/JointsWP-master/assets/images/green-commerce-entity-picture.png";
	const img = feature.properties.pictureUrl !== undefined
		? apiImgPath
			: feature.properties.greenCommerce
				? greenCommerceImgPath
				: defaultImgPath;

  const contents = `
		<div id="punts" data-url="${articleUrl}" class="punts ficha-punts">
		  <div class="entity-slider-box-header">
			<img src="${img}">
		  </div>
		  <h3>${feature.properties.name ? feature.properties.name : ''}</h3>
		  <div class="entity-region">
			  <p>${feature.properties.town ? feature.properties.town : ''}</p>
		  </div>
		  <div class="entity-slider-box-sectors">
			<div class="entity-slider-box-icons">
				<p><b>${sector}</b></p>
			</div>
		  </div>
		  <div class="entity-slider-box-contents">
			<div class="entity-slider-box-description">
				<p>${feature.properties.description ? feature.properties.description : '100'}</p>
			</div>
			<div class="entity-slider-box-button">
			${visitaperf}${greenCommerceBadge}
			</div>
		  </div>
		</div>`;
	return contents;
}


function refreshMap(data) {
	console.log("refreshMap")
	markers.clearLayers();
	sidebar.hide();
	map.setView(mapCenter, mapZoom, {
		"animate": true,
		"pan": {
			"duration": 3
		}
	});
	data = Array.from(data, ([name, value]) => ( value ));
	var leafletPoints = L.geoJson(data, {
		pointToLayer: function(feature, latlng) {
			var iconUrl = feature.properties.greenCommerce ? "/wp-content/themes/JointsWP-master/assets/images/green-commerce-icon.png" : jsBaseResourceUrl +feature.properties.sectorMapIconUrl;
			var leafletIcon = this.getLeafletIcon(iconUrl);
			return L.marker(latlng, {
				icon: leafletIcon
			})
			.on('mouseover', function() {
				var popup = L.popup({
						closeButton: false
					})
					.setContent(feature.properties.name);

				this.bindPopup(popup).openPopup();
			})
			.on('mouseout', function() {
				this.closePopup();
			})
			.on('click', function() {
				// se crea la info de la iniciativa, que mostrará al hacer click
				var details = doGetDetails(feature.properties.id);
				sidebar.setContent(getSidebarContents(details));
				sidebar.show();
				updateDotdotdot();
			});
		}
	});

	markers.addLayer(leafletPoints);

}

function searchLocation(e) {
	L.marker(e.latlng).addTo(map);
}

function doSearch() {
	var url = jQuery('#searchEntitiesForm').attr("action");
	var territoryId ;
	var territoryType ;
	var selectedSectors = [];
	jQuery('#searchSectors option').each(function() {
		if (this.selected === true) {
			selectedSectors.push(this.value);
		}
	});

	jQuery('.searchSectors input').each(function() {
		if (this.checked) {
			selectedSectors.push(this.value);
		}
	});

	jQuery('#searchSubsectors option').each(function() {
		if (this.selected === true) {
			console.log(`selectedSectors: ${selectedSectors}`);
			//we want to substitute all sectors by subsectors if they are present for that sector
			let parent = jQuery(`#subsector-${this.value}`).data("parent");
			console.log(`parent: ${parent}`);
			selectedSectors = selectedSectors.filter(item => item != parent);
			console.log(`selectedSectors after filter: ${selectedSectors}`);
			selectedSectors.push(this.value);
		}
	});

	var selectedTerritory = jQuery('#searchTerritory option:selected').val();
	if (selectedTerritory) {
		var territoryParts = selectedTerritory.split('-');
		territoryId = territoryParts[0];
		territoryType = territoryParts[1];
	}

	jQuery('#searchText').attr("placeholder", 'Paraula clau');

	var greenCommerceSelected = false;
	try{
		greenCommerceSelected = $("#greenCommerce").prop('checked') === true;
	} catch (error) {
	}

	sendData = {
		'text' : jQuery('#searchText').val(),
		'sectorIds' : selectedSectors,
		'territoryId' : territoryId,
		'territoryType' : territoryType,
		'greenCommerce': greenCommerceSelected,
		'refererDomain' : refererDomain,
		'apiKey' : apiKey
	};
	console.log(`sendData: ${JSON.stringify(sendData)}`);
	jQuery.ajax({
		url: url,
		type: "POST",
		data: JSON.stringify(sendData),
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data) {
			enabledCheckBox();
			if ( data.response.length == 0 ){
				jQuery('#searchText').attr("placeholder", 'Aquesta cerca no té resultats');
				jQuery('#searchText').val("");
			} else {
				entitesMap = arrayToMap(data.response);
				refreshMap(entitesMap);
			}
		}
	});
}

function doGetDetails(id) {
	var response = "{}"
	jQuery.ajax({
		url: jsBaseApiUrl + "/entitiesGeojson/" + id,
		type: "GET",
		async: false,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data) {
			response = data.response;
		}
	});

	return response;
}

var noSectorIcon = new L.icon({
	iconUrl: 'icones/sm-no-sector.png',
	iconSize: [34, 47],
	iconAnchor: [17, 46],
	popupAnchor: [0, -40]
});

var leafletIcons = {};

// Agrega la capa humanitaria
var tiles = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors',
	maxZoom: 18
});

var markers = new L.MarkerClusterGroup({
	maxClusterRadius: 20
});

var map = null;
var sidebar = null;

var selectedSectorsMap = new Map();

jQuery(document).ready(function() {

	jQuery('.search-box-panel').show('fast');

	map = L.map('map', {
		center: mapCenter,
		zoom: mapZoom,
		zoomControl: false
	});

	map
		.addLayer(tiles) // añadimos el mapa
		.addLayer(markers) // añadimos los marcadores del ClusterGroup
		.scrollWheelZoom.disable(); // scroll disable, (enable cuando haces click)

	map.on('click', function() {
		if (map.scrollWheelZoom.enabled()) {
			map.scrollWheelZoom.disable();
		} else {
			map.scrollWheelZoom.enable();
		}
	});

	L.control.zoom({
		position: 'topright'
	}).addTo(map);

	sidebar = L.control.sidebar('sidebar', {
		closeButton: true,
		position: 'right'
	});

	jQuery('#ok-search').click(function(){
		setContents();
		doSearch();
	});
	jQuery("#reset").click(function() {
		doSearch();
	});

	//on enter press do the same as when clicking on form button
	jQuery(document).on('keypress',function(e) {
		if(e.which == 13) {
			doSearch();
		}
	});

	jQuery('#searchText').mouseover(function(){
		map.doubleClickZoom.disable();
	});
	jQuery('#searchText').mouseout(function(){
		map.doubleClickZoom.enable();
	});
	jQuery('#searchText').mousedown(function(){
		map.dragging.disable();
	});
	jQuery(document).mouseup(function(){
		map.dragging.enable();
	});

	map.addControl(sidebar);

	jQuery('#geolocalitzacio').click(function(ev) {
		map.on('locationfound', searchLocation);
		map.locate({setView: true, maxZoom:12});
	});

	jQuery("#searchEntitiesForm").submit(function(event) {
		event.preventDefault();
		console.log('SearchEntities')
		doSearch();
	});

	//jQuery("#searchEntitiesForm").submit();

	jQuery("#selFiltres").click(function(event) {
		event.preventDefault();
		jQuery('.listSector').toggle("slow");
	});

	jQuery(".searchSectors input").change(function() {
        disabledCheckBox();
		if(this.checked) {
			selectedSectorsMap.set(this.id,this.value);
		} else {
			selectedSectorsMap.delete(this.id);
		}
		doSearch();
	});

	updateDotdotdot();

	jQuery('.search-menu').on('click', function() {
		jQuery('#formText').toggleClass("hide-for-small-only");
		jQuery('#geolocalitzacio').toggleClass("hide-for-small-only");
	});
    jQuery('#formText').on('click', function() {
        jQuery('#searchText').focus();

    });
    doSearch();

    jQuery('#sidebar').on('click', function() {
		var origin   = window.location.origin;
		var url = jQuery(this).children().attr("data-url") // will return the string "123"
		window.open(origin+url);
		console.log(origin+url);
	});
});

function disabledCheckBox() {
	jQuery(".searchSectors input").attr("disabled", true);
	jQuery("#preload").show();
}
function enabledCheckBox() {
    jQuery(".searchSectors input").removeAttr("disabled");
    jQuery("#preload").hide();

}
jQuery(document).keyup(function (e) {
	//input controll enter
	if (jQuery("#searchText").is(":focus") && (e.keyCode == 13)) {
    	jQuery('#formText').toggleClass("hide-for-small-only");
	}
});
