function updateDotdotdot() {
	$('.ellipsis').dotdotdot({
		ellipsis : '...',
		wrap : 'word'
	});
}

function getSendData(page, size) {
	var sendData = {
		action : "entities_grid",
		page : page,
		size : size
	}

	var text = $('#searchText').val();
	if (text) {
		sendData.text = text;
	}

	var selectedSectors = [];
	$("#searchSectors option").each(function() {
		if (this.selected === true) {
			selectedSectors.push(this.value);
		}
	});
	if (selectedSectors.length > 0) {
		sendData.sectorIds = selectedSectors;
	}

	var selectedSubsectors = [];
	$("#searchSubsectors option").each(function() {
		if (this.selected === true) {
			selectedSubsectors.push(this.value);
		}
	});
	if (selectedSubsectors.length > 0) {
		sendData.sectorIds = selectedSubsectors;
	}

	var selectedTerritory = $('#searchTerritory option:selected').val();
	if (selectedTerritory) {
		var territoryParts = selectedTerritory.split('-');
		sendData.territoryId = territoryParts[0];
		sendData.territoryType = territoryParts[1];
	}

	if (text || selectedSectors.length > 0 || selectedTerritory) {
		$('#reset').css('display', 'inline-block');
	}

	sendData.greenCommerce = $("#greenCommerce").prop('checked');

	return sendData;
}

function moreContents(page, size) {
	var url = $('#searchEntitiesForm').attr("action");
	$('.loading').css('display', 'flex');
	$('#more-contents').css('display', 'none');
	$.ajax({
		type : "POST",
		url : "/wp-admin/admin-ajax.php",
		data : getSendData(page, size),
		success : function(data) {
			data = JSON.parse(data);
			$('#entities-next-page').replaceWith(data.content);
			$('.loading').css('display', 'none');
			$('#more-contents').css('display', 'block');
			updateDotdotdot();
		}
	});
}

function setContents(page, size) {
	$('.loading').css('display', 'flex');
	$.ajax({
		type : "POST",
		url : "/wp-admin/admin-ajax.php",
		data : getSendData(page, size),
		success : function(data) {
			data = JSON.parse(data);
			if(data == null| data.entities == null || data.entities.length === 0){
				$("#punts").empty();
				$("#punts").append("<div class='listSector'><h5>Lamentablement, no s'han trobat entitats que coincideixin amb la teva cerca. Si us plau, intenta-ho de nou amb altres termes o verifica l'ortografia</h5></div>");
			} else {
				$("#punts").empty();
				$('#punts').append(data.content);
			}
			$('.loading').css('display', 'none');
			updateDotdotdot();
			$('#punts .featured-image a img, #punts a').hover(function() {
				var outWidget = $(this).parents('.panel');
				$(outWidget).addClass("visualEffect");
			});
			$('#punts .featured-image a img, #punts a').mouseout(function() {
				var outWidget2 = $(this).parents('.panel');
				$(outWidget2).removeClass("visualEffect");
			});
			var article = getUrlParameter("article");
			if (article !== undefined) {
				$('html, body').scrollTop($("#post-"+article).offset().top - 200);
			}
		}
	});
}

function replaceLocation(page, articleId){
	old_location = window.location.href;
	new_location = updateQueryStringParameter(old_location, 'article', articleId);
	new_location = updateQueryStringParameter(new_location, 'pag', page);
	window.history.pushState({}, '', new_location);
}

function updateQueryStringParameter(uri, key, value){
	var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
	var separator = uri.indexOf('?') !== -1 ? "&" : "?";
	if (uri.match(re)){
		return uri.replace(re, '$1' + key + "=" + value + "$2");
	} else {
		return uri + separator + key + "=" + value;
	}
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

jQuery(document).ready(function() {

	window.updateContents = true;

	document.getElementById("searchEntitiesForm2").addEventListener("keydown", function(event) {
		if (event.key === "Enter") {
			if (window.updateContents) {
				$("#punts").empty();
				setContents();
			}
			doSearch();
			event.preventDefault();
		}
	});

	$("#searchEntitiesForm").submit(function(event) {
		event.preventDefault();
		$("#punts").empty();
		var old_page = getUrlParameter("pag");

		if (old_page !== undefined){

			var size = (parseInt(old_page) + 1) * 12;
			setContents(0, size);
		} else {
			setContents();
		}
	});

	//$("#searchEntitiesForm").submit();

	$("#searchTerritory").SumoSelect({
		placeholder : 'Territoris',
		captionFormat : '{0} Sel·leccionats',
		captionFormatAllSelected : '{0} tots sel·leccionats!',
		search : true
	});

	$("#searchSectors").SumoSelect({
		placeholder : 'Sectors',
		captionFormat : '{0} Sel·leccionats',
		captionFormatAllSelected : '{0} tots sel·leccionats!',
		search : true
	});

	$("#searchSubsectors").SumoSelect({
		placeholder : 'Subsectors',
		captionFormat : '{0} Sel·leccionats',
		captionFormatAllSelected : '{0} tots sel·leccionats!',
		search : true
	});

	$("#ok-search").click(function(){
		if (window.updateContents) {
			$("#punts").empty();
			setContents();
		}
	});


	$("#reset").click(function() {
		window.updateContents = false;
		$('#searchText').val("");
		unselectSearchTerritory()
			.then(() => unselectSearchSectors());
		$("#punts").empty();
		setTimeout(() => {
			setContents();
			doSearch();
		}, 1000)
		window.updateContents = true;
	});

	const unselectSearchSectors = () => {
		return new Promise((resolve) => {
			setTimeout(() => {
				$("#searchSectors")[0].sumo.unSelectAll();
			}, 100)
		});
	}

	const unselectSearchTerritory = () => {
		return new Promise((resolve) => {
			setTimeout(() => {
				resolve($("#searchTerritory")[0].sumo.unSelectAll())
			}, 100)
		});
	}

	updateDotdotdot();
	$("section.sectors > p").dotdotdot({
		height : 65,
		fallbackToLetter : true,
		watch : true
	});

	$("#punts").empty();
	setContents();

});
