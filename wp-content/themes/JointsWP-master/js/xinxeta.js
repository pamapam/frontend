function refreshTerritory(territorySelect, territories) {
	var options = territorySelect.prop('options');
	territorySelect.empty();
	options[0] = new Option("", "");
	territories.forEach(function (region) {
		options[options.length] = new Option(region.name, region.id);
	});
}

function refreshSelect(select, items, addBlank) {
	var options = select.prop('options');
	select.empty();
	if (addBlank && options) {
		options[0] = new Option("", "");
	}
	if(options){
		items.forEach(function (item) {
			options[options.length] = new Option(item.name, item.id);
		});
	}

}

function loadSectors() {
	var url = jsBaseApiUrl + '/mainSectors';
	$.get(url, function(data) {
		refreshSelect($('#f_sector'), data, true);
	});
}

function loadLegalForms() {
	var url = jsBaseApiUrl + '/legalForms';
	$.get(url, function(data) {
		refreshSelect($('#f_legalForm'), data.response.content, true);
	});
}

function loadSocialEconomyNetworks() {
	var url = jsBaseApiUrl + '/socialEconomyNetworks';
	$.get(url, function(data) {
		refreshSelect($('#f_socialEconomyNetworks'), data.response.content, false);
	});
}

function loadProvinces() {
	var url = jsBaseApiUrl + '/provinces';
	$.get(url, function(data) {
		refreshTerritory($('#f_province'), data.response.content);
	});
}

function loadRegions(provinceId) {
	if (provinceId) {
		var url = jsBaseApiUrl + '/provinces/' + provinceId;
		$('#f_town').empty();
		$('#f_district').empty();
		$('#f_neighborhood').empty();
		$.get(url, function(data) {
			refreshTerritory($('#f_region'), data.response.regions);
		});
	}
}

function loadTowns(regionId) {
	if (regionId) {
		var url = jsBaseApiUrl + '/regions/' + regionId;
		$('#f_district').empty();
		$('#f_neighborhood').empty();
		$.get(url, function(data) {
			refreshTerritory($('#f_town'), data.response.towns);
		});
	}
}

function loadDistricts(townId) {
	if (townId) {
		var url = jsBaseApiUrl + '/towns/' + townId;
		$('#f_neighborhood').empty();
		$.get(url, function(data) {
			refreshTerritory($('#f_district'), data.response.districts);
		});
	}
}

function loadNeighborhoods(districtId) {
	if (districtId) {
		var url = jsBaseApiUrl + '/districts/' + districtId;
		$.get(url, function(data) {
			refreshTerritory($('#f_neighborhood'), data.response.neighborhoods);
		});
	}
}

jQuery(document).ready(function() {
	loadProvinces();
	loadSectors();
	loadLegalForms();
	loadSocialEconomyNetworks();

    $("#f_province").change(function() {
    	loadRegions(this.value);
    });

    $("#f_region").change(function() {
    	loadTowns(this.value);
    });

    $("#f_town").change(function() {
    	loadDistricts(this.value);
    });

    $("#f_district").change(function() {
    	loadNeighborhoods(this.value);
    });


	jQuery(".formulari-ajax").on("submit", function(e) {
		e.preventDefault();
		var $inputs =  $(".formulari-ajax :input");
		var data = {};
		$inputs.each(function() {
			data[this.name] = $(this).val();
		});

		$.ajax({
			url : "/wp-admin/admin-ajax.php",
			type:"POST",
			data: data,
			success: function(response){
				if(response === "true"){
					$(".success_msg").css("display","block");
					$(".error_msg").css("display","none");
					if(data.action === "add_iniciativa") {
						window.location.replace("/ok-ets-iniciativa//");
					}
					if(data.action === "add_xinxeta") {
						window.location.replace("/ok-xinxeta/");
					}
				} else {
					$(".error_msg").css("display","block");
					if(response === "#validation.user.username.exists"){
						$(".error_msg").html("Aquest nom d'usuari ja ha estat registrat");
					}
					if(response === "#validation.user.email.exists"){
						$(".error_msg").html("Aquest email ja ha estat registrat");
					}
				}
			}, error: function(data){
				$(".error_msg").css("display","block");
				$(".error_msg").html(data);
			},  beforeSend : function () {
				$(".filtered-formacions").html( "Carregant..." );
			}
		});
	});

});

window.onmousedown = function (e) {
    var el = e.target;
    if (el.tagName.toLowerCase() === 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();

        // toggle selection
        if (el.hasAttribute('selected')) el.removeAttribute('selected');
        else el.setAttribute('selected', '');

        // hack to correct buggy behavior
        var select = el.parentNode.cloneNode(true);
        el.parentNode.parentNode.replaceChild(select, el.parentNode);
    }
};


