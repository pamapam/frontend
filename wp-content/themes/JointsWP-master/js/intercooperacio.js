function refreshTerritory(territorySelect, territories) {
    var options = territorySelect.prop('options');
    territorySelect.empty();
    options[0] = new Option("", "");
    territories.forEach(function(region) {
        options[options.length] = new Option(region.name, region.id);
    });
}

function refreshSelect(select, items, addBlank) {
    var options = select.prop('options');
    select.empty();
    if (addBlank) {
        options[0] = new Option("", "");
    }
    items.forEach(function(item) {
        options[options.length] = new Option(item.name, item.id);
    });
}

function loadMainSectors() {
    var url = jsBaseApiUrl + '/sectors';
    $.get(url, function(data) {
        refreshSelect($('#f_mainSector'), data, true);
    });
}

function loadSectors() {
    var url = jsBaseApiUrl + '/sectors';
    $.get(url, function(data) {
        refreshSelect($('#f_sectors'), data, false);
    });
}

function loadLegalForms() {
    var url = jsBaseApiUrl + '/legalForms';
    $.get(url, function(data) {
        refreshSelect($('#f_legalForm'), data.response.content, true);
    });
}

function loadSocialEconomyNetworks() {
    var url = jsBaseApiUrl + '/socialEconomyNetworks';
    $.get(url, function(data) {
        refreshSelect($('#f_socialEconomyNetworks'), data.response.content, false);
    });
}

function loadProvinces() {
    var url = jsBaseApiUrl + '/provinces';
    $.get(url, function(data) {
        refreshTerritory($('#f_province'), data.response.content);
    });
}

function loadRegions(provinceId) {
    if (provinceId) {
        var url = jsBaseApiUrl + '/provinces/' + provinceId;
        $('#f_town').empty();
        $('#f_district').empty();
        $('#f_neighborhood').empty();
        $.get(url, function(data) {
            refreshTerritory($('#f_region'), data.response.regions);
        });
    }
}

function loadTowns(regionId) {
    if (regionId) {
        var url = jsBaseApiUrl + '/regions/' + regionId;
        $('#f_district').empty();
        $('#f_neighborhood').empty();
        $.get(url, function(data) {
            refreshTerritory($('#f_town'), data.response.towns);
        });
    }
}

function loadDistricts(townId) {
    if (townId) {
        var url = jsBaseApiUrl + '/towns/' + townId;
        $('#f_neighborhood').empty();
        $.get(url, function(data) {
            refreshTerritory($('#f_district'), data.response.districts);
        });
    }
}

function loadNeighborhoods(districtId) {
    if (districtId) {
        var url = jsBaseApiUrl + '/districts/' + districtId;
        $.get(url, function(data) {
            refreshTerritory($('#f_neighborhood'), data.response.neighborhoods);
        });
    }
}

function filesAdded(uploader, files) {
    $.each(files, function() {
        var img = new mOxie.Image();
        img.onload = function() {
            $('#preview').empty();
            this.embed($('#preview').get(0), {
                width: 400,
                height: 400
            });
        };
        img.onembedded = function() {
            this.destroy();
        };
        img.onerror = function() {
            this.destroy();
        };
        img.load(this.getSource());
    });
};

jQuery(document).ready(function() {
    loadProvinces();
    loadMainSectors();
    loadSectors();
    loadLegalForms();
    loadSocialEconomyNetworks();

    $("#f_province").change(function() {
        loadRegions(this.value);
    });

    $("#f_region").change(function() {
        loadTowns(this.value);
    });

    $("#f_town").change(function() {
        loadDistricts(this.value);
    });

    $("#f_district").change(function() {
        loadNeighborhoods(this.value);
    });

    var uploader = new plupload.Uploader({
        runtimes: 'html5',
        browse_button: 'selectfiles',
        multi_selection: false,
        url: "about:blank",
        filters: [{ title: "Image files", extensions: "jpg,gif,png" }]
    });

    uploader.init();
    uploader.bind("FilesAdded", filesAdded);

    $("#intercoop-form").submit(function(e) {
        var pictureData = $('#preview canvas')[0].toDataURL();
        $('#f_picture').val(pictureData);
    });

});

window.onmousedown = function(e) {
    var el = e.target;
    if (el.tagName.toLowerCase() == 'option' && el.parentNode.hasAttribute('multiple')) {
        e.preventDefault();

        // toggle selection
        if (el.hasAttribute('selected')) el.removeAttribute('selected');
        else el.setAttribute('selected', '');

        // hack to correct buggy behavior
        var select = el.parentNode.cloneNode(true);
        el.parentNode.parentNode.replaceChild(select, el.parentNode);
    }
}