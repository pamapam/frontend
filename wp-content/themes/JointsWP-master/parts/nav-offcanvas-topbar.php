<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<div id="top-bar-menu" class="top-bar row">
	<div id="logo" class="clearfix col-xs-12 col-md-3">
		<a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/pamapam-logo-bn.png" alt="Logo Pam a Pam" /></a>
	</div>
	<div class="menu-centered show-for-medium col-xs-12 col-md-9">
		<?php joints_top_nav(); ?>
	</div>
	<div class="top-bar-right float-right show-for-small-only col-xs-12 col-md-9">
		<ul class="menu">
			<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul>
	</div>
</div>
