<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	<header class="article-header">
		<a class="imatge" href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="articleBody">

<?php echo strip_tags(get_content_without_code(35)); ?>
	</section> <!-- end article section -->

	<footer class="article-footer">
	</footer> <!-- end article footer -->
</article> <!-- end article -->
