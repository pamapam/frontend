<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
	<header class="article-header">
		<a class="imatge" href="<?php the_permalink() ?>"><?php the_post_thumbnail('full'); ?></a>
		<?php get_template_part( 'parts/content', 'byline' ); ?>
		<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	</header> <!-- end article header -->

	<section class="entry-content" itemprop="articleBody">
		<?php echo get_excerpt(155); ?>
		<?php //echo strip_tags(get_content_without_code(35)); ?>
	</section> <!-- end article section -->

	<footer class="article-footer">
    	<p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->
</article> <!-- end article -->
