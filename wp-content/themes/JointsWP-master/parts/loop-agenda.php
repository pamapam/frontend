	<div>

		<div class="bloc-xinxeta">
			<div class="large-4 medium-12 small-12">
				<?php the_post_thumbnail('full'); ?>
			</div>
			<div id="main" class="large-8 medium-8 small-12 single-xinxeta" role="main">
					<?php
						$categories = get_the_terms($post->ID, 'tipus'); //obting el tipus d'activitats
						if ($categories) { // si hi ha tipus d'activitats
							$llista_categories = array(); // creo una variable per guardar la llista
							foreach ($categories as $categoria) {
								array_push($llista_categories, $categoria->name);
							}
						$llista = implode($llista_categories, ' - ');
						echo "<span class='llista_categories'>$llista</span>"; // i la mostro
						}
					?>
				<h2><?php the_title(); ?></h2>
				<div class="large-12 medium-12 small-12">
					<div id="dades">
						<ul class="dades_agenda">
						<li class="lloc"><strong>Lloc:</strong> <?php echo get_post_meta($post->ID, 'lloc', true); ?></li>
						<?php
						$data_agenda= get_post_meta($post->ID, 'data', true);
						$dia_agenda = substr($data_agenda,6,2);
						$mes_agenda = substr($data_agenda,4,2);
						$any_agenda = substr($data_agenda,0,4);
						?>
						<li class="data"><strong>Data:</strong> <?php echo $dia_agenda; ?>/<?php echo $mes_agenda; ?>/<?php echo $any_agenda; ?></li>

						<li class="hora"><strong>Hora d'inici:</strong> <?php echo get_post_meta($post->ID, 'hora_inici', true); ?></li>
						<li class="hora"><strong>Hora de finalització:</strong> <?php echo get_post_meta($post->ID, 'hora_fi', true); ?></li>
						<li class='adrecat'><strong>Adreçat a:</strong> <?php echo get_post_meta($post->ID, 'adrecat_a', true); ?></li>
					</ul>
					</div>
					<section class="entry-content" itemprop="articleBody">
				        <?php  the_content() ?>
					</section>
					<?php $inscriute = get_post_meta($post->ID, 'inscripcio', true); ?>
					<?php if (!empty($inscriute)) { ?>
						<a class="button" href="<?php echo $inscriute; ?>"> Inscriu-t’hi</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

