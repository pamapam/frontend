<?php
/*
Template Name: Fitxa un punt (No Sidebar)
*/
?>

<?php

//global $punt;
$punt_crudo =
'{
    "Nombre" : "L\'Olivera Cooperativa",
    "Imagen": "http://pamapam.cat/sites/default/files/styles/punt/public/fitxers/user95/captura_de_pantalla_2014-10-21_a_las_15.32.03.png",
    "Mapa": "pendiente",
    "Sectores": ["Roba", "Alimentació"],
    "Descripcion": "És una cooperativa d\'integració social que incorpora persones amb discapacitat psíquica que participen activament en el procés de cultiu i elaboració de vinya i oli. Va ser iniciada al 1974 a Vallbona de les Monges (Urgell), ofereix olis i vins que cerquen expressar el seu orígen: una terra i una gent. Els vins i olis ecològics que ofereixen es poden comprar online i a Barcelona hi tenen un punt de recollida.",
    "Direccion1": "Carrer Premià, 15, baixos",
    "Direccion2": "Sants-Montjuïc",
    "Horario": "De dilluns a divendres de 9:00 a 15:30 h",
    "Telefono": "931804066",
    "Mail": "olivera@olivera.org",
    "Facebook": "pendiente",
    "Twitter": "pendiente",
    "Criterios":  [
            ["Proximitat", 3],
            ["Comerç just", 4],
            ["Transparència", 0],
            ["Integració social", 5],
            ["Intercooperació", 3],
            ["Finances ètiques", 4],
            ["Criteris ecològics", 3],
            ["Gestió residus", 2],
            ["Eficiència energètica", 0],
            ["Desenvolupament personal", 4],
            ["Equitat de gènere", 3],
            ["Democàcia interna", 4],
            ["Programari lliure", 1]

        ]
}';

$punt = json_decode($punt_crudo);
?>
<?php get_header(); ?>
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">

                        <div id="puntcol1" class="large-4 medium-4 columns">
				<img src="<?php print($punt->Imagen) ?>" />
				aquí el mapa
			</div>

                        <div id="puntcol2" class="large-4 medium-4 columns">
				<h1 class="page-title"><?php print($punt->Nombre) ?></h1>
                                <?php

                                    foreach($punt->Sectores as $sector) {
                                        echo "<div id='puntsector'>$sector</div>";
                                    }

                                ?>

                                 <?php
                                    echo "<div id='puntdescripcio'>$punt->Descripcion</div>";
                                    echo "<div id='puntdireccio1'>$punt->Direccion1</div>";
                                    echo "<div id='puntdireccio2'>$punt->Direccion2</div>";
                                    echo "<div id='punthorari'>$punt->Horario</div>";
                                    echo "<div id='puntmail'>$punt->Mail</div>";
                                    echo "<div id='punttelefon'>Tel. $punt->Telefono</div>";
                                    echo "<div id='puntxarxes'>Xarxes<br />$punt->Twitter $punt->Facebook</div>";
                                 ?>
			</div>

                        <div id="puntcol3" class="large-4 medium-4 columns">
                                <div id="puntcriteris">
                                <p>Criteris</p>
                                <?php
                                    foreach($punt->Criterios as $criterio) {
                                        if ($criterio[1]==0) {
                                            echo "<div id='puntcriterio' class='cero'>$criterio[0]</div>";
                                        } else {
                                            echo "<div id='puntcriterio'>$criterio[0]";
                                            for($i=0;$i<$criterio[1];$i++) {
                                                echo "* ";
                                            }
                                            echo "</div>";
                                        }
                                    }

                                ?>
                                </div>
                                <p>Pam a Pam bla bla bla</p>
			</div>


			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
