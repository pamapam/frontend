<?php get_header(); ?>

<div id="content">

          <div id="inner-content" class="container-fluid">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-md-12">
                  <div class="box-head">
                     <h1 class="page-title"><?php the_archive_title(); ?></h1>

                            <?php echo addVesAlMapa(); ?>
                  </div>
                </div>
              </div>
            </div>

		<div class="container">
			<div class="row">
	    		<main id="main" class="col-xs-12 col-lg-9 blog" role="main">
	    			<div class="row">
		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		    		<div class="col-xs-12 col-lg-6">
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive' ); ?>
					</div>
				<?php endwhile; ?>
					<div class="col-xs-12 col-md-12">
					<?php joints_page_navi(); ?>
					</div>
				<?php else : ?>

					<?php get_template_part( 'parts/content', 'missing' ); ?>

				<?php endif; ?>
			</div>
				</main> <!-- end #main -->
    			<?php get_sidebar(); ?>
    		</div>
    	</div>
	</div> <!-- end #inner-content -->
</div> <!-- end #content -->


<?php get_footer(); ?>