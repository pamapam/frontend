<?php /* Template Name: DIAGNOSTICS */ ?>

<?php get_header(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/activitats-landing.js"></script>

  <div id="content">

    <div id="inner-content">
          <?php if(has_post_thumbnail()): ?>
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
          <?php else : ?>
            <div class="box-head">
                <h1 class="page-title"><?php the_title(); ?></h1>
                <?php echo addVesAlMapa(); ?>
            </div>
          <?php endif; ?>

<!-- Loop de consulta a la base de dades  marga-- >

<?php
 $queryPost = array( 'post_type'=> 'diagnostic',
     'post_status'=> 'publish',
     'posts_per_page'=> '10',
     'paged' => get_query_var('paged')
   )
?>
<!--
'meta_key'			=> 'data',
'orderby'			=> 'meta_value',
'order'				=> 'DESC'-->

<!-- fin loop de consulta a la base de dades  marga-- >

      <!-- inici llista -->
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-md-12">
          <?php query_posts($queryPost);?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <?php get_template_part( 'parts/loop', 'diagnostic' ); ?>
            <?php endwhile; ?>
            <div class="large-12 medium-12 columns ">
              <?php joints_page_navi(); ?>
            </div>
          <?php endif; wp_reset_query(); ?>
        </div>
      </div>
    </div>
      <!-- fi llista -->

  </div>
</div>





<!-- inici footer eric-->

<?php get_footer(); ?>

<!-- fi footer -->
