<?php /* Template Name: Afegeix punt intercooperació */ ?>


<?php
get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/intercooperacio.js"></script>
<!--
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>
-->
<script>
    <?php
    echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
    echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
    $scope = get_field('ambit-iniciativa');
    $externalFilterTags = get_field('filtres-mapeig');
    ?>
</script>

<div id="content">

          <div id="inner-content" class="container-fluid">
            <div class="container">
            <div class="row">
              <div class="col-xs-12 col-md-12">
                <div class="box-head">
                   <h1 class="page-title"><?php the_title(); ?></h1>
                          <?php echo addVesAlMapa(); ?>
                </div>
              </div>
            </div>
        </div>

                  <div class="container">
                    <div id="cap-formulari" class="row">
                      <?php if(has_post_thumbnail()): ?>
                        <div class="col-xs-12 col-md-6">
                            <div class="img-formulari">
                                <?php the_post_thumbnail('full'); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <?php the_content(); ?>
                        </div>
                      <?php else : ?>
                        <div class="col-xs-12 col-md-12">
                            <?php the_content(); ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>


        <div class="container">
            <div class="row">
            <div class="col-xs-12 col-lg-12">
                <form id="intercoop-form" action="" class="xinxeta-form" method="POST">
                    <!--DADES BÀSIQUES-->
                    <div class="row">
                        <div class="large-12 columns">
                            <p><strong>DADES BÀSIQUES</strong></p>
                            <input id="f_scope" type="hidden" name="f_scope" value="<?php echo $scope; ?>" />
                            <input id="f_externalFilterTags" type="hidden" name="f_externalFilterTags" value="<?php echo $externalFilterTags; ?>" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Nom del Pol</p>
                            <input id="f_name" type="text" name="f_name" class="input" required>
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">NIF/CIF</p>
                            <input id="f_nif" type="text" name="f_nif" class="input">
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Breu descripció del Pol (Propòsit de l'enxarxament, Projecte i serveis conjunts)</p>
                            <textarea id="f_description" name="f_description" cols="40" rows="5" class="descripcio"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Any de creació</p>
                            <input id="f_creation_date" type="text" name="f_creation_date" class="input" required>
                        </div>
                    </div>

                    <div class="row entitats" style="padding-top: 5rem; padding-bottom: 5rem;">
                        <div class="large-10 columns">
                            <p class="form-label">ENTITATS DEL POL</p>
                        </div>

                        <div class="large-2 columns">
                            <button type="button" class="button add-polcoop">+</button>
                        </div>


                        <div id="nom-link-tuple">
                            <div class="large-6 columns">
                                <p class="form-label">Nom</p>
                                <input type="text" name="f_entity_nom[]" class="input">
                            </div>

                            <div class="large-6 columns">
                                <p class="form-label">Link</p>
                                <input type="text" name="f_entity_link[]" class="input">
                            </div>
                        </div>

                        <div id="nom-link-tuple">
                            <div class="large-6 columns">
                                <p class="form-label">Nom</p>
                                <input type="text" name="f_entity_nom[]" class="input">
                            </div>

                            <div class="large-6 columns">
                                <p class="form-label">Link</p>
                                <input type="text" name="f_entity_link[]" class="input">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Projectes i/o serveis del pol </p>
                            <input type="text" id="f_productTags" name="f_productTags" placeholder="Etiqueta1, Etiqueta2, Etiqueta3 (amb commes)">
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Fotografia (<a href="" type="button" id="selectfiles">Trieu l'arxiu</a>)</p>
                            <div id="preview"></div>
                            <input id="f_picture" type="hidden" name="f_picture">
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Sector i subsector d'activitat principal del Pol cooperatiu</p>
                            <select id="f_mainSector" name="f_mainSector">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Altres sectors d'activitat del Pol (no de les iniciatives membre)</p>
                            <small>Si ho desitjeu, podeu sel·leccionar més d'una opció fent click sobre totes les opcions que vulgueu afegir</small>
                            <select id="f_sectors" name="f_sectors[]" multiple>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-12 columns">
                            <p class="form-label">Forma jurídica</p>
                            <select id="f_legalForm" name="f_legalForm">
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Província</p>
                            <select id="f_province" name="f_province">
                            </select>
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">Comarca</p>
                            <select id="f_region" name="f_region">
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label" style="margin-top: 23.5px;">Població</p>
                            <select id="f_town" name="f_town">
                            </select>
                        </div>
                        <div class="large-12 columns">
                            <p class="form-label">Altres xarxes amb les que es col.labora</p>
                            <input type="text" id="f_collaboratingWith" name="f_collaboratingWith" placeholder="Xarxa1, Xarxa2, Xarxa3 (amb commes)">
                        </div>
                    </div>


                    <!-- CONTACTE -->
                    <div class="row">
                        <div class="large-12 columns">
                            <p><strong>CONTACTE</strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-8 columns">
                            <p class="form-label">Avinguda, carrer, plaça...</p>
                            <input id="f_address" type="text" name="f_address" class="input">
                        </div>
                        <div class="large-4 columns">
                            <p class="form-label">Número</p>
                            <input id="f_number" type="text" name="f_number" class="input">
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Codi postal</p>
                            <input id="f_postalCode" type="text" name="f_postalCode" class="input">
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">Correu elèctronic de contacte</p>
                            <input id="f_email" type="mail" name="f_email" class="input" required pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$">
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Lloc web</p>
                            <input id="f_web" type="url" name="f_web">
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">Twitter</p>
                            <input id="f_twitter" type="url" name="f_twitter">
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-6 columns">
                            <p class="form-label">Facebook</p>
                            <input id="f_facebook" type="url" name="f_facebook">
                        </div>
                        <div class="large-6 columns">
                            <p class="form-label">Instagram</p>
                            <input id="f_instagram" type="url" name="f_instagram">
                        </div>
                    </div>

                    <div class="row">
                        <div class="large-6 columns">
                            <button type="submit" name="pol-coop-submit" class="button-submit button" value="">
                                <span>Afegiu-me al mapa</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
</div>
<script type="text/javascript">
    $(document).foundation();

    $(".add-polcoop").click(function () {
        $('.entitats').append($("#nom-link-tuple").clone().find("input").val("").end());
    });

    $(".add-projecte").click(function () {
        $('.etiquetes').append($("#projecte")
            .clone()
            .find("input")
            .val("")
            .end());
    });
</script>

<?php get_footer(); ?>