<?php get_header(); ?>

  <div id="content">
          <div id="inner-content" class="container-fluid">
      <div class="container">
        <div class="row">
          <main id="main" class="ok col-xs-12 col-lg-12" role="main">
                <h1 class="text-center"><?php _e( 'Error 404 - No hem trobat el què busques', 'jointswp' ); ?></h1>
                <p class="text-center"><?php _e( 'La pàgina que estàs buscant no funciona o ara mateix no és accessible, intenta-ho més tard.', 'jointswp' ); ?></p>
                <img src="<?php echo get_template_directory_uri()?>/assets/images/pamapam-logo-bn.png" />

                <a href="<?php echo get_site_url();?>" class="button button-large">Tornar a la pàgina principal</a>
            </div>
          </main>
        </div>
      </div>

    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->

<?php get_footer(); ?>