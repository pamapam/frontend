<!doctype html>
<!--comentario prueba git -->
<html class="no-js" <?php language_attributes(); ?>>
<head>
<meta charset="utf-8">
<!-- Force IE to use the latest rendering engine available -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta class="foundation-mq">
<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
			<!-- Icons & Favicons -->
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
<!--[if IE]>
				<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
			<![endif]-->
<meta name="msapplication-TileColor" content="#f01d4f">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
<meta name="theme-color" content="#121212">
	    <?php } ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php wp_head(); ?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57808927-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-57808927-1');
</script>
<!-- end analytics -->
<!-- Matomo --> 
<script> 
  var _paq = window._paq = window._paq || []; 
  _paq.push(['trackPageView']); 
  _paq.push(['enableLinkTracking']); 
  (function() { 
    var u="//matomo.xes.cat/"; 
    _paq.push(['setTrackerUrl', u+'matomo.php']); 
    _paq.push(['setSiteId', '3']); 
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; 
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s); 
  })(); 
</script>
<!-- End Matomo Code -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- filtres voluntaris xinxetes con MixItUp-->
<!--<script src="https://demos.kunkalabs.com/mixitup/mixitup.min.js"></script>-->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup/mixitup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup/mixitup-pagination.min.js"></script>
<!-- fin filtres voluntaris xinxetes -->
<!-- multifiltre per a punts -->
<!-- Requiere /js/mixitup/mixitup.min.js cargado más arriba. Alerta, no cambiar el orden -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/mixitup/mixitup-multifilter.min.js"></script>
<!-- fin paginar punts -->
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/dotdotdot/jquery.dotdotdot.min.js"></script>
</head>
<!-- Uncomment this line if using the Off-Canvas Menu -->
<body <?php body_class(); ?>>
	<div class="off-canvas-wrapper">

			<?php get_template_part( 'parts/content', 'offcanvas' ); ?>

			<div class="off-canvas-content" data-off-canvas-content>
			<header class="header container-fluid" role="banner">
				<div id="bar" class="col-xs-12 col-md-12">
					<div id="sessio" class="sessio col-xs-12 col-md-12">
						<a class="button gestio" href="<?php global $backofficeUrl; echo $backofficeUrl; ?>" target="_blank">Àrea de gestió</a>
						<a class="xarxes" href="https://www.instagram.com/pamapamcat" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/instagram.png" /></a>
						<a class="xarxes" href="https://www.facebook.com/pamapamCAT" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/facebook.png" /></a>
						<a class="xarxes" href="https://twitter.com/pamapamcat" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/twitter.png" /></a>
					</div>
					 <?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>
				</div>
			</header>
			<!-- end .header -->
