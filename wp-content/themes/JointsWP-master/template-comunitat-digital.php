<?php /* Template Name: Comunitat Digital */ ?>

<?php get_header(); ?>
  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="col-xs-12 col-md-12">
              <div class="box-head">
                <div class="col-xs-12 col-md-12">
                 <h1 class="page-title"><?php the_title(); ?></h1>

                        <?php echo addVesAlMapa(); ?>
                      </div>
              </div>
            </div>

         <?php endif; ?>
          <main id="main" class="container" role="main">
            <div class="row">
            <div class="col-xs-12 col-md-12">


              <div class="large-12 columns">
                <p class="parrafo-g"><?php the_field('text_a_sota_de_la_foto'); ?></p>
              </div>

              <div class="large-12 medium-12 small-12 columns">
                <h4 class="txt-grey"><?php the_field('titol_llista'); ?></h4>
                <ul><?php the_field('llista'); ?></ul>
              </div>
              <div class="large-12 medium-12 small-12 columns">
                <?php the_field('text_a_sota_de_la_llista'); ?>
              </div>


    <div class="large-12 medium-12 small-12 columns block-mercat">
      <div class="large-6 medium-6 small-12 columns mercat-left"><?php the_field('titol_mercat_social'); ?></div>
      <div class="large-6 medium-6 small-12 columns mercat-right"><?php the_field('text_mercat_social'); ?></div>

    </div>
</div>
            </div>
          </main> 


    <!---- Notícies d’economies comunitàries ---->
    <div class="container">
      <div class="row">
      <main id="main" class="col-xs-12 col-lg-12 blog" role="main">
        <div class="title">
          <h4>Notícies</h4>
        </div>
        <div class="noticies-intercoop row">
            <?php query_posts( array( 'category__and' => array(388), 'posts_per_page' => 3) );?>
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
          <div class="col-xs-12 col-lg-4">
                    <?php get_template_part( 'parts/loop', 'archive' ); ?>
                  </div>
        <?php endwhile; ?>
        <?php endif; wp_reset_query(); ?>
      </div>



        <!-- enllaç a veure més noticies -->
      
        <!--
          <nav class="page-navigation row">
            <div class="col-xs-12 col-md-12">
              <a class="button" href="/ca/blog/category/comunitat-digital">Veure més notícies</a>
            </div>
          </nav>
        -->

        </main> <!-- end #main -->
      </div>
    </div>


    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->



<?php get_footer(); ?>
