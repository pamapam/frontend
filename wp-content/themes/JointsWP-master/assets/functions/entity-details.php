<!-- Directori Article -- PUNTS -->
<div class="col-xs-12 col-lg-8">
	<div class="row" style="align-items: center;">
		<div class="col-xs-12 col-lg-5">
		    <?php
		    $apiImgPath = $baseApiUrl . $entity->pictureUrl;
				$defaultImgPath = $entity->greenCommerce
					? get_site_url() . "/wp-content/themes/JointsWP-master/assets/images/green-commerce-entity-picture.png"
					: get_template_directory_uri()."/assets/images/default_PAP.jpg";
					$img = $entity->pictureUrl ? $apiImgPath :  $defaultImgPath;

		    ?>
			<div class="ima-pagina-ficha"><img src="<?php echo $img; ?>" /></div>
		</div>
		<div class="col-xs-12 col-lg-7">
			<ul class="llista-sectors">
			<?php
			    $apiImgPath = $baseApiUrl . $entity->mainSector->iconUrl;
			    $defaultImgPath = get_template_directory_uri()."/assets/images/default_PAP.jpg";
			    $img = $entity->mainSector->iconUrl ?$apiImgPath: $defaultImgPath;
			?>

			<li><img width="45px" class="disabled" src="<?php echo $img; ?>" title="<?php echo $entity->mainSector->name; ?>" />
			<b><?php echo $entity->mainSector->name; ?></b></li>
			<?php
			foreach($entity->sectors as $sector) {
			?>
			<li><img class="disabled" src="<?php echo $baseApiUrl . $sector->iconUrl; ?>" title="<?php echo $sector->name; ?>" />
			<?php echo $sector->name; ?></li>
			<?php
			}
			?>
			</ul>
		</div>
	</div>
<main id="main" class="single-xinxeta" role="main">
<article role="article">
	<section class="entry-content" itemprop="articleBody">
		<p><?php echo $entity->description; ?></p>
	</section>

    <?php if($entity->isIntercooperacio) {?>
        <section class="entry-content description ellipsis info" itemprop="articleBody">
            <?php echo ($entity->legalForm->name) ?: ""; ?>
        </section>
        <section class="entry-content description ellipsis info" itemprop="articleBody">
            Constituïda des de <?php echo ($entity->foundationYear) ? $entity->foundationYear : ''; ?>
        </section>
    <section class="entry-content description ellipsis padding-top-10" itemprop="articleBody">
        <?php
        $polCoopNotEmpty = [!is_null($entity),
            isset($entity->intercooperacioLinks),
            !is_null($entity->intercooperacioLinks),
            $entity->intercooperacioLinks];

        if (!in_array(false, $polCoopNotEmpty)) {?>
            <p><?php echo "Formen part d'aquesta iniciativa: ";?></p>
            <?php foreach ($entity->intercooperacioLinks as $link) {?>
               <li><a href="<?php echo $link->url; ?>" target="_blank"><?php echo ($link->name) ? : $link->url;?></a></li>
            <?php }?>
        <?php }?>
    </section>
    <?php }?>

	<hr>
	<section class="adress">
		<div class="vcard">
			<?php if (!empty($entity->address)) { ?>
				<p>
					<strong>Adreça:</strong><br><!--<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;--><?php echo $entity->address; ?>. <?php //echo $entity->neighborhood->name; ?> <?php echo $entity->town->name; ?>.
				</p>
			<?php } ?>
<?php
			if (!empty($entity->openingHours)) {
?>
			<p class="schedule"><strong>Horari:</strong><br><!--<i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;--><?php echo $entity->openingHours; ?></p>
<?php
			}
?>
<?php
			if (!empty($entity->phone)) {
?>
			<p><strong>Telèfon:</strong><br>
			<!--<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;--><?php echo $entity->phone; ?></p>
<?php
			}
?>
<?php
			if (!empty($entity->email)) {
?>
			<p><strong>Correu electrònic:</strong><br><!--<i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;--><?php echo $entity->email; ?></p>
<?php
			}
?>
<?php
			if (!empty($entity->web)) {
?>
			<p><strong>Web:</strong><br><!--<i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp;--><a href="<?php echo $entity->web; ?>" target="_blank"><?php echo $entity->web; ?></a></p>
<?php
			}
?>
<?php
			if (!empty($entity->onlineShoppingWeb)) {
?>
				<p><strong>Venda en línia:</strong><br><!--<i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;&nbsp;--><a href="<?php echo $entity->onlineShoppingWeb; ?>" target="_blank"><?php echo $entity->onlineShoppingWeb; ?></a></p>
<?php
			}
			if (!empty($entity->laZonaWeb)) {
?>
				<p><strong>Venda a La Zona:</strong><br><!--<i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;<i class="pamapam-font pamapam-icon-lazona" aria-hidden="true" style="vertical-align: -5%;"></i>&nbsp;&nbsp;--><a href="<?php echo $entity->laZonaWeb; ?>" target="_blank"><?php echo $entity->laZonaWeb; ?></a></p>
<?php
			}
?>

		</div>
	</section>
		<?php
		$xarxes = array(
		  $entity->facebook,
		  $entity->twitter,
		  $entity->googlePlus,
		  $entity->instagram,
		  $entity->pinterest,
		  $entity->quitter
		);

		// Comprobar si todos los elementos del array están vacíos
		if (array_filter($xarxes, 'strlen')) {

				?>
			<div class="social">
				<strong>Xarxes</strong><br/>
				<?php
						if (!empty($entity->facebook)) {
				?>
							<span class="fn"><a href="<?php echo $entity->facebook; ?>"target="_blank"><span class="icon-facebook"></span></a></span>
				<?php
						}
						if (!empty($entity->twitter)) {
				?>
							<span class="twitter"><a href="<?php echo $entity->twitter; ?>"target="_blank"><span class="icon-twitter"></span></a></span>
				<?php
						}
						if (!empty($entity->googlePlus)) {
				?>
							<span class="googleplus"><a href="<?php echo $entity->googlePlus; ?>"target="_blank"><span class="icon-google-plus"></span></a></span>
				<?php
						}
						if (!empty($entity->instagram)) {
				?>
							<span class="instagram"><a href="<?php echo $entity->instagram; ?>"target="_blank"><span class="icon-instagram"></span></a></span>
				<?php
						}
						if (!empty($entity->pinterest)) {
				?>
							<span class="pinterest"><a href="<?php echo $entity->pinterest; ?>"target="_blank"><span class="icon-pinterest"></span></a></span>
				<?php
						}
						if (!empty($entity->quitter)) {
				?>
							<span class="quitter"><a href="<?php echo $entity->quitter; ?>"target="_blank"><span class="icon-quitter"></span></a></span>
				<?php
						}
				?>
			</div>
		<?php } ?>
	<hr>
</article>
</main>
</div>

<div class="col-xs-12 col-lg-4 sidebar-criteris">

	<?php
    $evaluationNotEmpty = [!is_null($entity),
        isset($entity->entityEvaluation),
        !is_null($entity->entityEvaluation)];
	if(empty($entity->entityStatus->entityStatusType == "EXTERNAL")): ?>

		<h3>Sistemes d'avaluació</h3>
		<hr>

		<p class="titol-sidebar">Criteris Pam a Pam</p>
		<ul class="criteris">
			<?php
					$criterionLevels = array();
					if ($entity->entityEvaluation) {
						foreach ($entity->entityEvaluation->entityEvaluationCriterions as $entityEvaluationCriterion) {
							$criterionLevels[$entityEvaluationCriterion->criterion->id] = $entityEvaluationCriterion->level;
						}
					}

					$imageWidth = "width=\"24\"";
					if ($entity->oldVersion) {
						$imageWidth = "";
					}

					foreach ($criteria as $criterion) {
						$criterionLevel = $criterionLevels[$criterion->id];
						if ($criterionLevel === NULL) {
			?>
					<li class='cero'><img class="disabled" src="<?php echo $baseApiUrl . $criterion->iconUrl; ?>" <?php echo $imageWidth; ?>/><p><?php echo $criterion->name; ?></p></li>
				<?php
				} else if ($criterionLevel < 0) {
					//no es fa res, el criteri no aplica, no es mostra

				} else if ($criterionLevel == 0) {
				?>
					<li>
						<div class="crit">
							<img class="disabled" src="<?php echo $baseApiUrl . $criterion->iconUrl; ?>" <?php echo $imageWidth; ?>/>
							<p><?php echo $criterion->name; ?></p>
						</div>
					</li>
				<?php
				} else {
				?>
					<li>
						<div class="crit">
							<img src="<?php echo $baseApiUrl . $criterion->iconUrl; ?>" <?php echo $imageWidth; ?>/>
							<p><?php echo $criterion->name; ?></p>
						</div>
						<div class="dotis">
							<svg viewBox="0 0 120 120" version="1.1" xmlns="https://www.w3.org/2000/svg">
	  							<circle cx="60" cy="60" r="50"/>
							</svg>
							<svg viewBox="0 0 120 120" version="1.1" xmlns="https://www.w3.org/2000/svg">
	  							<circle cx="60" cy="60" r="50"/>
							</svg>
							<svg viewBox="0 0 120 120" version="1.1" xmlns="https://www.w3.org/2000/svg">
	  							<circle cx="60" cy="60" r="50"/>
							</svg>
							<svg viewBox="0 0 120 120" version="1.1" xmlns="https://www.w3.org/2000/svg">
	  							<circle cx="60" cy="60" r="50"/>
							</svg>
							<svg viewBox="0 0 120 120" version="1.1" xmlns="https://www.w3.org/2000/svg">
	  							<circle cx="60" cy="60" r="50"/>
							</svg>
						</div>
						<div class="dots">
					<?php
					$dotsCount = round($criterionLevel / 100, 0, PHP_ROUND_HALF_DOWN);
					for ($i = 0; $i < $dotsCount; $i++) {
					?>
							<span class="do"></span>
					<?php } ?>
						</div>
					<?php }} ?>

		</ul>
	<?php else: ?>
		<?php if(!$entity->xesBalance && !$entity->greenCommerce): ?>
		<div id="extern" class="container">
			<p class="orange">Aquest punt no ha passat cap sistema d'avaluació amb criteris de l'ESS per entrar a Pam a Pam</p>
		</div>
		<?php endif ?>
	<?php if($entity->xesBalance || $entity->greenCommerce): ?>
		<h3>Sistemes d'avaluació</h3>
		<hr>
	<?php endif ?>
	<?php endif ?>



	<?php if($entity->xesBalance): ?>
		<div id="b-social" class="container">
			<img class="center" src="/wp-content/themes/JointsWP-master/assets/images/balanc-s.png">
            <?php //el permalink 5992 es https://pamapam.cat/balanc-social/?>
			<p>Aquesta iniciativa ha fet el Balanç Social de la XES. <a href="<?php echo $entity->xesBalanceUrl ? $entity->xesBalanceUrl : esc_url( get_permalink(5992) ); ?>" target="_blank"><strong>Consulta'l</strong></a></p>
		</div>
	<?php else: ?>
		<!--
		<div id="extern" class="container">
			<p class="orange tit-bold">Pam a Pam és el mapa col·laboratiu de l'Economia Solidària a Catalunya. Una eina per a la transformació social, on mostrem iniciatives que compleixen els criteris d'ESS per facilitar-te el consum responsable.</p>
		</div>
		-->
	<?php endif ?>
	<?php if($entity->greenCommerce): ?>
		<div id="green-commerce" class="container">
			<img class="center" src="/wp-content/themes/JointsWP-master/assets/images/green-commerce-gray.png">
			<p>Aquesta iniciativa té la certificació Comerç Verd-Rezero. <a href="/comerc-verd" target="_blank"><strong>Consulta'l</strong></a></p>
		</div>
	<?php else: ?>
		<!--
		<div id="extern" class="container">
			<p class="orange tit-bold">Pam a Pam és el mapa col·laboratiu de l'Economia Solidària a Catalunya. Una eina per a la transformació social, on mostrem iniciatives que compleixen els criteris d'ESS per facilitar-te el consum responsable.</p>
		</div>
		-->
	<?php endif ?>

	</div>
