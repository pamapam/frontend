<div class="large-4 medium-6 columns">
    <?php
    $apiImgPath = $baseApiUrl . $entity->pictureUrl;
    $defaultImgPath = get_template_directory_uri()."/assets/images/default_PAP.jpg";
    $img = $entity->pictureUrl ?$apiImgPath: $defaultImgPath;
    ?>
    <div class="ima-pagina-ficha"><img src="<?php echo $img; ?>" /></div>
    <div class="overlay" onClick="style.pointerEvents='none'"></div>
    <div id="map" style="height: 400px;"></div>
    <script>
        var nombre = "<?php echo addslashes($entity->name); ?>";
        var marker1 = "<?php echo $entity->latitude; ?>";
        var marker2 = "<?php echo $entity->longitude; ?>";

        var osmUrl = "https://a.tile.openstreetmap.org/{z}/{x}/{y}.png";
        var osmAttrib = "Map data ©<a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors";
        var osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
        var map = L.map('map').setView([marker1, marker2], 17).addLayer(osm);
        L.marker([marker1, marker2])
            .addTo(map)
            .bindPopup(nombre)
            .openPopup();
    </script>
</div>
<main id="main" class="large-4 medium-4 columns single-xinxeta" role="main">
    <article role="article">
        <header class="article-header">
            <h1 class="entry-title single-title"><?php echo $entity->name; ?></h1>
        </header>
        <ul class="llista-sectors">
            <?php
            $apiImgPath = $baseApiUrl . $entity->mainSector->iconUrl;
            $defaultImgPath = get_template_directory_uri()."/assets/images/default_PAP.jpg";
            $img = $entity->mainSector->iconUrl ?$apiImgPath: $defaultImgPath;
            $img = check_if_remote_image_is_empty($apiImgPath, $img, $defaultImgPath)
            ?>
            <li><img class="disabled" src="<?php echo $img; ?>"<?php echo $entity->mainSector->name; ?>>
            <b><?php echo $entity->mainSector->name; ?></b></li>

            <?php
            foreach($entity->sectors as $sector) {
                ?>
                <li><img class="disabled" src="<?php echo $baseApiUrl . $sector->iconUrl; ?>"<?php echo $sector->name; ?>>
                    <?php echo $sector->name; ?></li>

                <?php
            }
            ?>
        </ul>
        <section class="entry-content" itemprop="articleBody">
            <p><?php echo $entity->description; ?></p>
        </section>
        <section class="entry-content description ellipsis" itemprop="articleBody">
            <?php echo ($entity->legalForm->name) ?: ""; ?>
        </section>
        <section class="entry-content description ellipsis" itemprop="articleBody">
            Constituïda des de <?php echo ($entity->foundationYear) ? $entity->foundationYear : ''; ?>
        </section>

        <hr>
        <div class="social">
            <strong>Xarxes</strong><br/>
            <?php
            if (!empty($entity->facebook)) {
                ?>
                <span class="fn"><a href="<?php echo $entity->facebook; ?>"target="_blank"><span class="icon-facebook"></span></a></span>
                <?php
            }
            if (!empty($entity->twitter)) {
                ?>
                <span class="twitter"><a href="<?php echo $entity->twitter; ?>"target="_blank"><span class="icon-twitter"></span></a></span>
                <?php
            }
            if (!empty($entity->googlePlus)) {
                ?>
                <span class="googleplus"><a href="<?php echo $entity->googlePlus; ?>"target="_blank"><span class="icon-google-plus"></span></a></span>
                <?php
            }
            if (!empty($entity->instagram)) {
                ?>
                <span class="instagram"><a href="<?php echo $entity->instagram; ?>"target="_blank"><span class="icon-instagram"></span></a></span>
                <?php
            }
            if (!empty($entity->pinterest)) {
                ?>
                <span class="pinterest"><a href="<?php echo $entity->pinterest; ?>"target="_blank"><span class="icon-pinterest"></span></a></span>
                <?php
            }
            if (!empty($entity->quitter)) {
                ?>
                <span class="quitter"><a href="<?php echo $entity->quitter; ?>"target="_blank"><span class="icon-quitter"></span></a></span>
                <?php
            }
            ?>
        </div>
    </article>
</main>

<div class="large-4 small-12 columns">
    <section class="adress">
        <div class="vcard">
            <p class="adr">
                <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;<span class="street-address"><?php echo $entity->address; ?></span>
                <span class="area"><?php echo $entity->neighborhood->name; ?></span><br>
                <span class="locality"><?php echo $entity->town->name; ?></span><br>
                <span class="region">(<?php echo $entity->province->name; ?>)</span><br>
            </p>


            <p class="schedule"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo $entity->openingHours; ?></p>
            <p>
                <i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp;<span class="tel"><?php echo $entity->phone; ?></span><br/>
                <i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp;<span class="email"><?php echo $entity->email; ?></span><br/>
                <i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp;<span class="web"><a href="<?php echo $entity->web; ?>" target="_blank"><?php echo $entity->web; ?></a></span>
                <?php
                if (!empty($entity->onlineShoppingWeb)) {
                    ?>
                    <br/><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;&nbsp;<span class="web"><a href="<?php echo $entity->onlineShoppingWeb; ?>" target="_blank"><?php echo $entity->onlineShoppingWeb; ?></a></span>
                    <?php
                }
                if (!empty($entity->laZonaWeb)) {
                    ?>
                    <br/><i class="fa fa-shopping-basket" aria-hidden="true"></i>&nbsp;<i class="pamapam-font pamapam-icon-lazona" aria-hidden="true" style="vertical-align: -5%;"></i>&nbsp;&nbsp;<span class="web"><a href="<?php echo $entity->laZonaWeb; ?>" target="_blank"><?php echo $entity->laZonaWeb; ?></a></span>
                    <?php
                }
                ?>
            </p>
        </div>
    </section>

    <section class="entry-content description ellipsis padding-top-10" itemprop="articleBody">
        <?php
        $polCoopNotEmpty = [!is_null($entity),
            isset($entity->intercooperacioLinks),
            !is_null($entity->intercooperacioLinks),
            $entity->intercooperacioLinks];

        if (!in_array(false, $polCoopNotEmpty)) {?>
            <hr>
            <p class="margin-bottom-none"><?php echo "Formen part d'aquesta iniciativa: ";?></p>
            <?php foreach ($entity->intercooperacioLinks as $link) {?>
                <li><a href="<?php echo $link->url; ?>" target="_blank"><?php echo ($link->name) ? : $link->url;?></a></li>
            <?php }?>
        <?php }?>
    </section>
</div>
