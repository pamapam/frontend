<!-- Punts del Directori Prinicpal -- Pàgina MAPA -->
<?php
$groupedSectors = [];
foreach ($entity->sectors as $sector) {
	echo var_dump($sector);
	$sectorNames = $groupedSectors[$sector->iconUrl];
	if ($sectorNames == null) {
		$sectorNames = $sector->name;
		$groupedSectors[$sector->iconUrl] = $sectorNames;
	} else {
		$sectorNames .= ', ' . $sector->name;
	}
}
$mainDivClass = 'mix ' . $sectorsString . ' ' . $entity->province->name;
?>

<div class="<?php echo $mainDivClass; ?> col-xs-12 col-md-6"
     data-search="<?php echo $entity->name . ' ' . $entity->description; ?>">
  <a target="_blank" onclick="replaceLocation('<?php echo $old_page; ?>', '<?php echo $entity->normalizedName; ?>');"
     href="/directori/<?php echo $entity->normalizedName; ?>">
    <div class="panel" data-equalizer-watch>
      <article id="post-<?php echo $entity->normalizedName; ?>" class="<?php echo join(' ', get_post_class('')); ?>"
               role="article">
        <div class="entity-box-header" style="display: flex;flex-direction: column;">
          <section class="featured-image" itemprop="articleBody">
              <?php
              $apiImgPath = $baseApiUrl . $entity->pictureUrl;
							$isGreenCommerce = $entity->greenCommerce;
							$greenCommerceImgPath = get_template_directory_uri() . "/assets/images/green-commerce-entity-picture.png";
							$defaultImgPath = $isGreenCommerce ? $greenCommerceImgPath : get_template_directory_uri() . "/assets/images/default_PAP.jpg";
              $arrayUrl = explode('/', $entity->pictureUrl);
              $img = (!empty($entity->pictureUrl) && end($arrayUrl) != 'null') ? $apiImgPath : $defaultImgPath;
              ?>
            <img src="<?php echo $img; ?>"/>
          </section>
          <header class="article-header">
            <h3 class="title">
              <strong><?php echo $entity->name; ?></strong>
            </h3>
            <p class="territori"><?php echo $entity->town->name; ?></p>
            <p class="sector"><strong><?php echo $entity->mainSector->name; ?></strong></p>
          </header>
          <section class="entry-content description ellipsis" itemprop="articleBody">
            <p>
                <?php
                if (strlen($entity->description) > 140) {
                    $short_desc = substr($entity->description, 0, 140);
                    $short_desc = substr($short_desc, 0, strrpos($short_desc, ' ')) . '...';
                } else {
                    $short_desc = $entity->description;
                }
                echo $short_desc;
                ?>
            </p>
          </section>
          <br>
          <section class="sectors" itemprop="articleBody">
            <ul class="llista-sectors">
                <?php
                $xinxeta = get_template_directory_uri() . "/assets/images/xinxeta1.png";
                $balance = get_template_directory_uri() . "/assets/images/b-social.png";
                ?>
                <?php build_balanc_and_pamapam_badges($entity);?>
            </ul>
          </section>
        </div>
          <?php
          call_criteria();
          ?>
      </article>
    </div>
  </a>
</div>
