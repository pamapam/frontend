jQuery(document).foundation();
/*
 * These functions make sure WordPress and Foundation play nice together.
 */

var headerHeight;

jQuery(document).ready(function() {

	headerHeight = jQuery('#main > div.large-12.small-12.columns.centered.menu-static').prop('offsetTop') + jQuery('#top-bar-menu').prop('offsetHeight') + jQuery('#main > div.box-head').prop('offsetHeight');
	// Remove empty P tags created by WP inside of Accordion and Orbit
	jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass('end');

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if (jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5) {
			jQuery(this).wrap("<div class='widescreen flex-video'/>");
		} else {
			jQuery(this).wrap("<div class='flex-video'/>");
		}
	});

	jQuery('#punts .featured-image a img, #punts a').hover(function() {
		var outWidget = $(this).parents('.panel');
		jQuery(outWidget).addClass("visualEffect");
	});
	jQuery('#punts .featured-image a img, #punts a').mouseout(function() {
		var outWidget2 = $(this).parents('.panel');
		jQuery(outWidget2).removeClass("visualEffect");
	});

	jQuery('.search-box-panel').show('fast');

	/* Aixo serveix per minimitzar la capcalera fent scroll */



//script to create sticky header
jQuery(function () {
  createSticky(jQuery("#top-bar-menu"));
});

function createSticky(sticky) {
  if (typeof sticky != "undefined") {
    var pos = sticky.offset().top + 250,
      win = jQuery(window);

    // scroll should really be debounced
    win.on("scroll", function () {
      if (win.scrollTop() > pos) {
        sticky.addClass("specialfixed");
      } else {
        sticky.removeClass("specialfixed");
      }
    });
  }
}
	/* Aixo serveix per minimitzar el menu estatic de la pagina quisom fent scroll */
//	jQuery(window).scroll(function() {
//		var nav = jQuery('.menu-static');
//		var thisContent = jQuery('.page-template-template-queespamapam section.entry-content');
//	});

	/* Aixo serveix per posar una clase al mapa fent scroll */
//	jQuery(window).scroll(function() {
//		var nav = jQuery('.home #map');
//		var boxContent = jQuery('.home #inner-content > #main');
//		var mapIframe = $("iframe#map-iframe");
//	});

	/* Aixo serveix per fixar els input al fer scroll a la home */
//	jQuery(window).scroll(function() {
//		var filters = jQuery('.search-box-panel');
//
//		if (jQuery(this).scrollTop() > 80) {
//			filters.addClass("special-fixed");
//		} else {
//			filters.removeClass("special-fixed");
//			return false;
//		}
//
//	});

	//Sistema para hacer anchor en una misma página con animación

	jQuery('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]').not('[href="#0"]').click(function(event) {
		// Get the height of the header
		// On-page links
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			// Figure out element top scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				console.log('click: ' + headerHeight);
				console.log('scroll: ' + target.offset().top);
				jQuery('html, body').animate({
				//                scrollTop: target.offset().top-headerHeight
				}, 1000, function() {
					// Callback after animation
					// Must change focus!
					//var $target = $(target);
					//$target.focus();
					// if ($target.is(":focus")) { // Checking if the target was focused
					// return false;
					// } else {
					//  $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
					//  $target.focus(); // Set focus again
					//  };
					return false;
				});
			}
		}
	}); /* END */
});
