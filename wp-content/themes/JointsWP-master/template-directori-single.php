<?php /* Template Name: SINGLE Directori*/ ?>

<?php
$url = $_SERVER ["REQUEST_URI"];
$entity_id = $wp_query->get('entityId');
$entity = call_entity();

if ($entity == null) {
	wp_redirect(home_url());
	exit();
}

$criteria = call_criteria();

function swap_seo_title(){
	global $entity;
	return "Pam a Pam | $entity->name";
}
add_filter("wpseo_opengraph_title", "swap_seo_title");
add_filter("wpseo_title", "swap_seo_title");
?>

<?php get_header(); ?>
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
	integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ==" crossorigin=""></link>
<script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
	integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log==" crossorigin=""></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/directori-single.js"></script>

	<div id="content">
		<div id="inner-content">
			<div class="box-head row">
					<div class="col-xs-12 col-lg-12">
					 <h1 class="page-title title"><?php echo $entity->name; ?></h1>

	                <?php echo addVesAlMapa(); ?>
	            	</div>
			</div>
			<div class="info-punts row">
				<?php entity_single(); ?>
			</div>
		</div>
</div>

<?php get_footer(); ?>