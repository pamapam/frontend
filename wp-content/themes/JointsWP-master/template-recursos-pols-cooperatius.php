<?php /* Template Name: Recursos Pols cooperatius */ ?>

<!-- inici menú -->

<?php get_header(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/activitats-landing.js"></script>
<div class="row upper-mar">
<div class="box-head">
  <h1 class="page-title"><?php the_title(); ?></h1>
  <?php echo addVesAlMapa(); ?>
</div>
</div>

<!-- fi menú-->


<!-- Loop de consulta a la base de dades  marga-- >

<?php
 $queryPost = array( 'post_type'=> 'recurs_pol_cooperati',
     'post_status'=> 'publish',
     'posts_per_page'=> '10',
     'paged' => get_query_var('paged')
   )
?>
<!--
'meta_key'			=> 'data',
'orderby'			=> 'meta_value',
'order'				=> 'DESC'-->

<!-- fin loop de consulta a la base de dades  marga-- >

<!-- inici llista -->


<div id="content">
	<div id="inner-content" class="row"> <!-- aquesta div fa marges al contingut. potser s'haurà de posar dins del filtre(?) -->

    <?php query_posts($queryPost);?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php get_template_part( 'parts/loop', 'diagnostic' ); ?>
      <?php endwhile; ?>
      <div class="large-12 medium-12 columns ">
        <?php joints_page_navi(); ?>
      </div>
    <?php endif; wp_reset_query(); ?>
  </div>
</div>

<!-- fi llista -->


<!-- inici xarxes socials eric-->

<!-- banners afiliacions socials -->
  <div class="social-afiliations">

    <div class="row">
      <div class="large-6 small-12 columns">

        <div class="large-12 columns in">
          <div class="celito purple"></div>

          <p class="large-7 small-12 columns">segueix-nos</p>
          <ul class="large-5 small-12 columns">
            <li><a href="https://www.facebook.com/pamapamCAT/" target="_blank"><span class="icon-facebook"></span></a></li>
            <li><a href="https://twitter.com/pamapamcat?lang=es" target="_blank"><span class="icon-twitter"></span></a></li>
            <li><a href="https://www.instagram.com/pamapamcat/" target="_blank"><span class="icon-instagram"></span></a></li>
          </ul>
        </div>
      </div>
      <div class="large-6 small-12 columns">


        <div class="large-12 columns in">
          <div class="celito green"></div>

          <p class="large-7 small-12 columns">butlletí de notícies</p>
          <p class="large-5 small-12 columns"><a href="/butlleti/" class="">Apunta-t'hi <span class="icon-arrow-right"></span></a></p>
      </div>
      </div>
   </div>

 </div>
  <!-- end banners afiliacions socials -->

<!-- fi xarxes socials -->



<!-- inici footer eric-->

<?php get_footer(); ?>

<!-- fi footer -->
