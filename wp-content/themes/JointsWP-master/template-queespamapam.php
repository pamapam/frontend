<?php
/*
Template Name: Què és PAMAPAM (No Sidebar)
*/
?>
<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="box-head container">
						 <h1 class="page-title"><?php the_title(); ?></h1>
		                <?php echo addVesAlMapa(); ?>
					</div>
				</div>
			</div>

			<div class="row">
			    <main id="main" class="container" role="main">
	                <div class="col-xs-12 col-md-12">
	                    <?php //joints_static_nav(); ?>
	                    <?php the_content(); ?>
	                </div>
				</main> <!-- end #main -->
			</div> <!-- end #inner-content -->
		</div>
	</div> <!-- end #content -->
<?php get_footer(); ?>
