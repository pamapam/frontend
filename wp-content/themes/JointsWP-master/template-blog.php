<?php /* Template Name: Blog */ ?>

<!--Query Post-->
<?php
 $queryPost = array( 'post_type'=> 'post',
     'post_status'=> 'publish',
     'posts_per_page'=> '10',
     'paged' => get_query_var('paged'))
?>

<?php get_header(); ?>

<div id="content">

    <div id="inner-content">
		<?php if(has_post_thumbnail()): ?>
		<div class="pagina-img-wrap">
		  <div class="pagina-title">
		    <h1><?php the_title(); ?></h1>
		  </div>
		  <?php the_post_thumbnail('full'); ?>
		</div>
		<?php else : ?>
		<div class="box-head">
		    <h1 class="page-title"><?php the_title(); ?></h1>
		    <?php echo addVesAlMapa(); ?>
		</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
	    <main id="main" class="col-xs-12 col-md-9 blog" role="main">
            <?php query_posts($queryPost);?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="col-md-12 col-lg-6">
                		<?php get_template_part( 'parts/loop', 'archive' ); ?>
                	</div>
				<?php endwhile; ?>
                <div class="col-xs-12 col-md-12">
                <?php joints_page_navi(); ?>
                </div>
		    <?php endif; wp_reset_query(); ?>
		</main> <!-- end #main -->
    	<?php get_sidebar(); ?>
    	</div>
    </div>
	</div> <!-- end #inner-content -->
</div> <!-- end #content -->


<?php get_footer(); ?>
