<?php
/*
Template Name: Full Width (No Sidebar)
*/
?>

<?php get_header(); ?>

  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
         <?php endif; ?>
      <div class="container">
        <div class="row">
          <main id="main" class="col-xs-12 col-lg-12" role="main">
              <?php the_content(); ?>
          </main>
        </div>
      </div>

    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->

<?php get_footer(); ?>