<?php /* Template Name: ETS-EINATECA */ ?>

<?php get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/ets-einateca.js"></script>
<!--
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>
-->
<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
?>
</script>

<div id="content">

          <div id="inner-content" class="container-fluid">
          	<div class="container">
	            <div class="row">
	              <div class="col-xs-12 col-md-12">
	                <div class="box-head">
	                   <h1 class="page-title"><?php the_title(); ?></h1>
	                          <?php echo addVesAlMapa(); ?>
	                </div>
	              </div>
	            </div>
	          </div>

			      <div class="container">
			        <div id="cap-formulari" class="row">
			          <?php if(has_post_thumbnail()): ?>
			            <div class="col-xs-12 col-md-6">
			                <div class="img-formulari">
			                    <?php the_post_thumbnail('full'); ?>
			                </div>
			            </div>
			            <div class="col-xs-12 col-md-6">
			              <?php the_content(); ?>
			            </div>
			          <?php else : ?>
			            <div class="col-xs-12 col-md-12">
			                <?php the_content(); ?>
			            </div>
			          <?php endif; ?>
			        </div>
			      </div>


        <div class="container">
            <div class="row">
					<form id="einateca-form" action= get_site_url() . "ok-ets-einateca/" . "add&noheader=true" class="xinxeta-form" method="post">
						<div class="row">

						<!--DADES BASISQUES-->
				<div class="col-xs-12 col-md-6">

						<div class="row">
							<div class="large-12 columns">
								<p><strong>DADES BÀSIQUES</strong></p>
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<p class="form-label">Nom de la xarxa</p>
								<input id="f_name" type="text" name="f_name" class="input" required>
							</div>
							<div class="large-6 columns">
								<p class="form-label">NIF/CIF de la xarxa</p>
								<input id="f_nif" type="text" name="f_nif" class="input">
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<p class="form-label">Breu descripció de la xarxa (data de creació, nombre de projectes membres, motius d'enxarxament, activitats desenvolupades)</p>
	                            <textarea id="f_description" name="f_description" cols="40" rows="5" class="descripcio"></textarea>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<p class="form-label">Què produiu, oferiu?</p>
								<input type="text" id="f_productTags" name="f_productTags" placeholder="<?php esc_attr_e ('Etiqueta1 Etiqueta2 Etiqueta3 (sense commas)');?>">
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<p class="form-label">Fotografia (<a href="" type="button" id="selectfiles">Trieu l'arxiu</a>)</p>
								<div id="preview"></div>
								<input id="f_picture" type="hidden" name="f_picture">
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<p class="form-label">Sector principal</p>
								<select id="f_mainSector" name="f_mainSector">
								</select>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<p class="form-label">Sectors</p>
								<select id="f_sectors" name="f_sectors[]" multiple>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<p class="form-label">Forma jurídica</p>
								<select id="f_legalForm" name="f_legalForm">
								</select>
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<p class="form-label">Província</p>
								<select id="f_province" name="f_province">
								</select>
							</div>
							<div class="large-6 columns">
								<p class="form-label">Comarca</p>
								<select id="f_region" name="f_region">
								</select>
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<p class="form-label" style="margin-top: 23.5px;">Població</p>
								<select id="f_town" name="f_town">
								</select>
							</div>	
							<div class="large-6 columns">
								<p class="form-label">Altres xarxes amb les que es col.labora</p>
								<textarea id="f_collaboratingWith" name="f_collaboratingWith" cols="40" rows="5" class="descripcio"></textarea>
							</div>					
						</div>
				</div>

						<!--ADREÇA-->

				<div class="col-xs-12 col-md-6">

						<div class="row">
							<div class="large-12 columns">
								<p><strong>CONTACTE</strong></p>
							</div>
						</div>
						<div class="row">
							<div class="large-8 columns">
								<p class="form-label">Avinguda, carrer, plaça...(en el cas d'una xarxa transversal, indiqueu l'adreça de la seu de la xarxa)</p>
								<input id="f_address" type="text" name="f_address" class="input" placeholder="<?php esc_attr_e ('Adreça');?>" required>
							</div>
							<div class="large-4 columns">
								<p class="form-label" style="margin-top: 22.5px;">Número</p>
								<input id="f_number" type="text" name="f_number" class="input" required>
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<p class="form-label">Codi postal</p>
								<input id="f_postalCode" type="text" name="f_postalCode" class="input">
							</div>
							<div class="large-6 columns">
								<p class="form-label">Correu elèctronic de contacte</p>
								<input id="f_email" type="mail" name="f_email" class="input" required pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$">
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<p class="form-label">Lloc web</p>
								<input id="f_web" type="url" name="f_web">
							</div>
							<div class="large-6 columns">
								<p class="form-label">Twitter</p>
								<input id="f_twitter" type="url" name="f_twitter">
							</div>
						</div>
						<div class="row">
							<div class="large-6 columns">
								<p class="form-label">Facebook</p>
								<input id="f_facebook" type="url" name="f_facebook">
							</div>					
							<div class="large-6 columns">
								<p class="form-label">Instagram</p>
								<input id="f_instagram" type="url" name="f_instagram">
							</div>						
						</div>					

						<!--AMBIT DE L'ECONOMIA SOCIAL I SOLIDARIA-->					
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
						<div class="row">
							<br>
							<div class="large-12 columns">
								<input type="checkbox" name="userConditionsAccepted" id="userConditionsAccepted" value="true" required>
									<span>Accepto i he llegit les <a href="/avis-legal/" target="_blank">Condicions d’ús</a> i la <a href="/politica-de-privacitat/" target="_blank">Política de privacitat</a></span>
								</input>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<input type="checkbox" name="newslettersAccepted" id="newslettersAccepted" value="true">
									<span>Accepto rebre notícies tal i com s’exposa <a href="/politica-de-privacitat/" target="_blank">aquí</a></span>
								</input>
							</div>
						</div>
						
						<div class="small-8 small-centered columns">
							<button type="submit" name="einateca-submit" class="button-submit button" value="">
								<span>Afegiu-me al mapa</span>
							</button>
						</div>

									</div>
				
			</div>

                </form>
        </div>
    </div>


</div>
</div>
<script type="text/javascript">
	$(document).foundation();
</script>
<?php get_footer(); ?>
