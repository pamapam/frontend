<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php');

/**
 ** Activar el item de menú dependiendo de la url. Añade una clase al item activo
 **/

// Backoffice connection URLs. Must be defined in wp-config.php

$baseApiUrl = BASE_API_URL;
$baseApiInternalUrl = BASE_API_INTERNAL_URL;
$backofficeUrl = BACKOFFICE_URL;


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class($classes, $item){
  // si es post o blog landing, es decir, si en la url está la palabra blog
  $blog_landing = strpos($_SERVER["REQUEST_URI"], 'blog');
  $es_blog = ( $blog_landing > 0 );

  if ($es_blog) {
    // activar el item de menú blog
    if($item->title == 'Blog'){
      $classes[] = 'current-menu-item active';
    }
  }
  return $classes;
}

// Adds directori-landing-functions
require_once(get_template_directory().'/assets/functions/directori-landing-functions.php');

add_action('wp_ajax_entities_grid', 'entities_grid');
add_action('wp_ajax_nopriv_entities_grid', 'entities_grid');

// Adds directori-single-functions
require_once(get_template_directory().'/assets/functions/directori-single-functions.php');

add_action('wp_ajax_entity_single', 'entity_single');
add_action('wp_ajax_nopriv_entity_single', 'entity_single');

// Adds comunitat-landing-functions
require_once(get_template_directory().'/assets/functions/comunitat-landing-functions.php');

add_action('wp_ajax_user_grid', 'user_grid');
add_action('wp_ajax_nopriv_user_grid', 'user_grid');

// Adds comunitat-single-functions
require_once(get_template_directory().'/assets/functions/comunitat-single-functions.php');

add_action('wp_ajax_entity_single', 'user_single');
add_action('wp_ajax_nopriv_entity_single', 'user_single');

// Adds inici-map-functions
require_once(get_template_directory().'/assets/functions/inici-mapa-functions.php');

add_action('wp_ajax_entities_map', 'entities_map');
add_action('wp_ajax_nopriv_entities_map', 'entities_map');


function dashboard_redirect(){
	$author = wp_get_current_user();
	if (isset($author->roles [0])) {
		$current_role = $author->roles [0];
	} else {
		$current_role = 'no_role';
	}

	if ($current_role == 'xinxeta' || $current_role == 'external') {
		wp_redirect(admin_url('edit.php'));
	}
}

add_action('load-index.php','dashboard_redirect');

function disqus_embed($disqus_shortname) {
	if (is_single()) {
		global $post;
		wp_enqueue_script('disqus_embed','http://'.$disqus_shortname.'.disqus.com/embed.js');
		echo '<div id="disqus_thread"></div>
		    <script type="text/javascript">
		        var disqus_shortname = "'.$disqus_shortname.'";
		        var disqus_title = "'.$post->post_title.'";
		        var disqus_url = "'.get_permalink($post->ID).'";
		        var disqus_identifier = "'.$disqus_shortname.'-'.$post->ID.'";
		    </script>';
	}
}

// ...	Disable Wordpress update notifications.
function hide_update_notice_to_all_but_admin_users() {
	if (!current_user_can('update_core')) {
		remove_action( 'admin_notices', 'update_nag', 3 );
	}
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );

add_action('wp_logout', 'wpoa_end_logout', 0);

// ends the logout request by redirecting the user to the desired page:
function wpoa_end_logout() {
	$_SESSION["WPOA"]["RESULT"] = 'Logged out successfully.';
	unset($_SESSION["WPOA"]["LAST_URL"]);
	wp_safe_redirect(site_url());
	die();
}
//Add dot in string
function add3dots($string, $repl, $limit) {
	if (strlen($string) > $limit) {
        return substr($string, 0, $limit) . $repl;
	} else {
        return $string;
    }
}

function addVesAlMapa (){
    $string = '<span class=link-to-map><a href="/ca/mapa" class="button"><p>VES AL MAPA</p><span class="icon-ic_room_48px"></span></a></span>';
    return $string ;
}

add_action("wp_loaded", "redirect_iniciativa_form");

function redirect_iniciativa_form (){
	if ( isset( $_POST["iniciativa-submit"])){

	global $baseApiInternalUrl;
	$url = $baseApiInternalUrl . "/addInitiative";

	$name = isset($_POST ['f_name']) ? $_POST ['f_name'] : null;
	$nif = isset($_POST ['f_nif']) ? $_POST ['f_nif'] : null;
 	$province = isset($_POST ['f_province']) ? $_POST ['f_province'] : null;
	$region = isset($_POST ['f_region']) ? $_POST ['f_region'] : null;
	$town = isset($_POST ['f_town']) ? $_POST ['f_town'] : null;
	$district = isset($_POST ['f_district']) ? $_POST ['f_district'] : null;
	$neighborhood = isset($_POST ['f_neighborhood']) ? $_POST ['f_neighborhood'] : null;
	$openingHours = isset($_POST ['f_openingHours']) ? $_POST ['f_openingHours'] : null;
	$productTags = isset($_POST ['f_productTags']) ? $_POST ['f_productTags'] : null;
	$address = isset($_POST ['f_address']) ? $_POST ['f_address'] : null;
	$number = isset($_POST ['f_number']) ? $_POST ['f_number'] : null;
	$email = isset($_POST ['f_email']) ? $_POST ['f_email'] : null;
	$phone = isset($_POST ['f_phone']) ? $_POST ['f_phone'] : null;
	$web = isset($_POST ['f_web']) ? $_POST ['f_web'] : null;
	$twitter = isset($_POST ['f_twitter']) ? $_POST ['f_twitter'] : null;
	$facebook = isset($_POST ['f_facebook']) ? $_POST ['f_facebook'] : null;
	$googlePlus = isset($_POST ['f_googlePlus']) ? $_POST ['f_googlePlus'] : null;
	$instagram = isset($_POST ['f_instagram']) ? $_POST ['f_instagram'] : null;
	$pinterest = isset($_POST ['f_pinterest']) ? $_POST ['f_pinterest'] : null;
	$quitter = isset($_POST ['f_quitter']) ? $_POST ['f_quitter'] : null;
	$description = isset($_POST ['f_description']) ? $_POST ['f_description'] : null;
	$mainSector = isset($_POST ['f_mainSector']) ? $_POST ['f_mainSector'] : null;
	$legalForm = isset($_POST ['f_legalForm']) ? $_POST ['f_legalForm'] : null;
	$socialEconomyNetworks = isset($_POST ['f_socialEconomyNetworks']) ? $_POST ['f_socialEconomyNetworks'] : null;
	$collaboratingWith = isset($_POST ['f_collaboratingWith']) ? $_POST ['f_collaboratingWith'] : null;
	$socialBalance = isset($_POST ['socialBalance']) ? $_POST ['socialBalance'] : null;
	$userConditionsAccepted = isset($_POST ['userConditionsAccepted']) ? $_POST ['userConditionsAccepted'] : null;
	$newslettersAccepted = isset($_POST ['newslettersAccepted']) ? $_POST ['newslettersAccepted'] : null;
	$scope = isset($_POST ['f_scope']) ? $_POST ['f_scope'] : null;
	$externalFilterTags = isset($_POST ['f_externalFilterTags']) ? $_POST ['f_externalFilterTags'] : null;

	$data = array (
			'name' => $name,
			'nif' => $nif,
			'provinceId' => $province,
			'regionId' => $region,
			'townId' => $town,
			'districtId' => $district,
			'neighborhoodId' => $neighborhood,
			'openingHours' => $openingHours,
			'productTags' => $productTags,
			'labels' => $labels,
			'address' => $address . $number,
			'email' => $email,
			'phone' => $phone,
			'web' => $web,
			'twitter' => $twitter,
			'facebook' => $facebook,
			'googlePlus' => $googlePlus,
			'instagram' => $instagram,
			'pinterest' => $pinterest,
			'quitter' => $quitter,
			'description' => $description,
			'mainSectorId' => $mainSector,
			'legalFormId' => $legalForm,
			'socialEconomyNetworkIds' => $socialEconomyNetworks,
			'collaboratingWith' => $collaboratingWith,
			'socialBalance' => $socialBalance,
			'userConditionsAccepted' => $userConditionsAccepted,
			'newslettersAccepted' => $newslettersAccepted,
			'scope' => $scope,
			'externalFilterTags' => $externalFilterTags
	);
	$jsondata = json_encode(array_filter($data));
	$options = array (
			'http' => array (
					'header' => "Content-type: application/json",
					'method' => 'POST',
					'content' => $jsondata
			)
	);

	$context = stream_context_create($options);
	$result = file_get_contents($url, false, $context);

	wp_redirect(home_url('/ok-iniciativa'));
	exit();
	}
}

add_action("wp_loaded", "redirect_einateca_form");

function redirect_einateca_form (){
	if ( isset( $_POST["einateca-submit"])){

		global $baseApiInternalUrl;
		$url = $baseApiInternalUrl . "/addEinatecaInitiative";

		$name = isset($_POST ['f_name']) ? $_POST ['f_name'] : null;
		$nif = isset($_POST ['f_nif']) ? $_POST ['f_nif'] : null;
		$province = isset($_POST ['f_province']) ? $_POST ['f_province'] : null;
		$region = isset($_POST ['f_region']) ? $_POST ['f_region'] : null;
		$town = isset($_POST ['f_town']) ? $_POST ['f_town'] : null;
		$district = isset($_POST ['f_district']) ? $_POST ['f_district'] : null;
		$neighborhood = isset($_POST ['f_neighborhood']) ? $_POST ['f_neighborhood'] : null;
		$openingHours = isset($_POST ['f_openingHours']) ? $_POST ['f_openingHours'] : null;
		$productTags = isset($_POST ['f_productTags']) ? $_POST ['f_productTags'] : null;
		$address = isset($_POST ['f_address']) ? $_POST ['f_address'] : null;
		$number = isset($_POST ['f_number']) ? $_POST ['f_number'] : null;
		$postalCode = isset($_POST ['f_postalCode']) ? $_POST ['f_postalCode'] : null;
		$email = isset($_POST ['f_email']) ? $_POST ['f_email'] : null;
		$phone = isset($_POST ['f_phone']) ? $_POST ['f_phone'] : null;
		$web = isset($_POST ['f_web']) ? $_POST ['f_web'] : null;
		$twitter = isset($_POST ['f_twitter']) ? $_POST ['f_twitter'] : null;
		$facebook = isset($_POST ['f_facebook']) ? $_POST ['f_facebook'] : null;
		$googlePlus = isset($_POST ['f_googlePlus']) ? $_POST ['f_googlePlus'] : null;
		$instagram = isset($_POST ['f_instagram']) ? $_POST ['f_instagram'] : null;
		$pinterest = isset($_POST ['f_pinterest']) ? $_POST ['f_pinterest'] : null;
		$quitter = isset($_POST ['f_quitter']) ? $_POST ['f_quitter'] : null;
		$description = isset($_POST ['f_description']) ? $_POST ['f_description'] : null;
		$mainSector = isset($_POST ['f_mainSector']) ? $_POST ['f_mainSector'] : null;
		$sectors = isset($_POST ['f_sectors']) ? $_POST ['f_sectors'] : null;
		$legalForm = isset($_POST ['f_legalForm']) ? $_POST ['f_legalForm'] : null;
		$socialEconomyNetworks = isset($_POST ['f_socialEconomyNetworks[]']) ? $_POST ['f_socialEconomyNetworks[]'] : null;
		$collaboratingWith = isset($_POST ['f_collaboratingWith']) ? $_POST ['f_collaboratingWith'] : null;
		$socialBalance = isset($_POST ['socialBalance']) ? $_POST ['socialBalance'] : null;
		$userConditionsAccepted = isset($_POST ['userConditionsAccepted']) ? $_POST ['userConditionsAccepted'] : null;
		$newslettersAccepted = isset($_POST ['newslettersAccepted']) ? $_POST ['newslettersAccepted'] : null;
		$picture = isset($_POST ['f_picture']) ? $_POST ['f_picture'] : null;

		$data = array (
				'name' => $name,
				'nif' => $nif,
				'provinceId' => $province,
				'regionId' => $region,
				'townId' => $town,
				'districtId' => $district,
				'neighborhoodId' => $neighborhood,
				'openingHours' => $openingHours,
				'productTags' => $productTags,
				'labels' => $labels,
				'address' => $address . ', ' . $number,
				'postalCode' => $postalCode,
				'email' => $email,
				'phone' => $phone,
				'web' => $web,
				'twitter' => $twitter,
				'facebook' => $facebook,
				'googlePlus' => $googlePlus,
				'instagram' => $instagram,
				'pinterest' => $pinterest,
				'quitter' => $quitter,
				'description' => $description,
				'mainSectorId' => $mainSector,
				'sectorsIds' => $sectors,
				'legalFormId' => $legalForm,
				'socialEconomyNetworkIds' => $socialEconomyNetworks,
				'collaboratingWith' => $collaboratingWith,
				'socialBalance' => $socialBalance,
				'userConditionsAccepted' => $userConditionsAccepted,
				'newslettersAccepted' => $newslettersAccepted,
				'picture' => $picture

		);
		$jsondata = json_encode(array_filter($data));
		$options = array (
				'http' => array (
						'header' => "Content-type: application/json",
						'method' => 'POST',
						'content' => $jsondata
				)
		);

		$context = stream_context_create($options);
		$result = file_get_contents($url, false, $context);

		wp_redirect(home_url('/ok-einateca'));
		exit();
	}
}

add_action("wp_loaded", "redirect_voluntaria_form");

function redirect_voluntaria_form (){
	if ( isset( $_POST["voluntaria-submit"])){
		global $baseApiInternalUrl;
    $url = $baseApiInternalUrl . "/addProposed";

    $name = isset($_POST ['f_name']) ? $_POST ['f_name'] : '';
    $province = isset($_POST ['f_province']) ? $_POST ['f_province'] : '';
    $region = isset($_POST ['f_region']) ? $_POST ['f_region'] : '';
    $town = isset($_POST ['f_town']) ? $_POST ['f_town'] : '';
    $district = isset($_POST ['f_district']) ? $_POST ['f_district'] : '';
    $neighborhood = isset($_POST ['f_neighborhood']) ? $_POST ['f_neighborhood'] : '';
    $address = isset($_POST ['f_address']) ? $_POST ['f_address'] : '';
    $number = isset($_POST ['f_number']) ? $_POST ['f_number'] : '';
    $email = isset($_POST ['f_email']) ? $_POST ['f_email'] : null;
    $phone = isset($_POST ['f_phone']) ? $_POST ['f_phone'] : null;
    $web = isset($_POST ['f_web']) ? $_POST ['f_web'] : null;
    $description = isset($_POST ['f_description']) ? $_POST ['f_description'] : '';
    $proposerName = isset($_POST ['f_proposerName']) ? $_POST ['f_proposerName'] : '';
    $proposerEmail = isset($_POST ['f_proposerEmail']) ? $_POST ['f_proposerEmail'] : '';
    $comments = isset($_POST ['f_comments']) ? $_POST ['f_comments'] : '';
    $scope = isset($_POST ['f_scope']) ? $_POST ['f_scope'] : null;
    $externalFilterTags = isset($_POST ['f_externalFilterTags']) ? $_POST ['f_externalFilterTags'] : null;

    $data = array (
            'name' => $name,
            'provinceId' => $province,
            'regionId' => $region,
            'townId' => $town,
            'districtId' => $district,
            'neighborhoodId' => $neighborhood,
            'address' => $address . ' ' . $number,
    		'email' => $email,
    		'phone' => $phone,
    		'web' => $web,
    		'description' => $description,
            'proposerName' => $proposerName,
            'proposerEmail' => $proposerEmail,
            'comments' => $comments,
    		'scope' => $scope,
    		'externalFilterTags' => $externalFilterTags
    );

    $jsondata = json_encode($data);
    $options = array (
            'http' => array (
                    'header' => "Content-type: application/json",
                    'method' => 'POST',
                    'content' => $jsondata
            )
    );

    $context = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    wp_redirect(home_url('/ok-proposta'));
    exit();
}
}

add_action("wp_loaded", "redirect_pol_coop_form");

function redirect_pol_coop_form (){
	if ( isset( $_POST["pol-coop-submit"])){
		global $baseApiInternalUrl;
    $url = $baseApiInternalUrl . "/addPolIntercoop";

        $name = isset($_POST ['f_name']) ? $_POST ['f_name'] : null;
        $creationDate = isset($_POST ['f_creation_date']) ? $_POST ['f_creation_date'] : null;
        $nif = isset($_POST ['f_nif']) ? $_POST ['f_nif'] : null;
        $province = isset($_POST ['f_province']) ? $_POST ['f_province'] : null;
        $region = isset($_POST ['f_region']) ? $_POST ['f_region'] : null;
        $town = isset($_POST ['f_town']) ? $_POST ['f_town'] : null;
        $district = isset($_POST ['f_district']) ? $_POST ['f_district'] : null;
        $neighborhood = isset($_POST ['f_neighborhood']) ? $_POST ['f_neighborhood'] : null;
        $openingHours = isset($_POST ['f_openingHours']) ? $_POST ['f_openingHours'] : null;
        $productTags = isset($_POST ['f_productTags']) ? $_POST ['f_productTags'] : null;
        $address = isset($_POST ['f_address']) ? $_POST ['f_address'] : null;
        $number = isset($_POST ['f_number']) ? $_POST ['f_number'] : null;
        $postalCode = isset($_POST ['f_postalCode']) ? $_POST ['f_postalCode'] : null;
        $email = isset($_POST ['f_email']) ? $_POST ['f_email'] : null;
        $phone = isset($_POST ['f_phone']) ? $_POST ['f_phone'] : null;
        $web = isset($_POST ['f_web']) ? $_POST ['f_web'] : null;
        $twitter = isset($_POST ['f_twitter']) ? $_POST ['f_twitter'] : null;
        $facebook = isset($_POST ['f_facebook']) ? $_POST ['f_facebook'] : null;
        $instagram = isset($_POST ['f_instagram']) ? $_POST ['f_instagram'] : null;
        $pinterest = isset($_POST ['f_pinterest']) ? $_POST ['f_pinterest'] : null;
        $quitter = isset($_POST ['f_quitter']) ? $_POST ['f_quitter'] : null;
        $description = isset($_POST ['f_description']) ? $_POST ['f_description'] : null;
        $mainSector = isset($_POST ['f_mainSector']) ? $_POST ['f_mainSector'] : null;
        $sectors = isset($_POST ['f_sectors']) ? $_POST ['f_sectors'] : null;
        $legalForm = isset($_POST ['f_legalForm']) ? $_POST ['f_legalForm'] : null;
        $socialEconomyNetworks = isset($_POST ['f_socialEconomyNetworks[]']) ? $_POST ['f_socialEconomyNetworks[]'] : null;
        $collaboratingWith = isset($_POST ['f_collaboratingWith']) ? $_POST ['f_collaboratingWith'] : null;
        $socialBalance = isset($_POST ['socialBalance']) ? $_POST ['socialBalance'] : null;
        $userConditionsAccepted = isset($_POST ['userConditionsAccepted']) ? $_POST ['userConditionsAccepted'] : null;
        $newslettersAccepted = isset($_POST ['newslettersAccepted']) ? $_POST ['newslettersAccepted'] : null;
        $picture = isset($_POST ['f_picture']) ? $_POST ['f_picture'] : null;
        $polCoopNames = isset($_POST ['f_entity_nom']) ? $_POST ['f_entity_nom'] : null;
        $polCoopLinks = isset($_POST ['f_entity_link']) ? $_POST ['f_entity_link'] : null;

        $polCoopTextUrls = build_pol_coop_text_urls($polCoopNames,$polCoopLinks);

        $data = array (
            'name' => $name,
            'nif' => $nif,
            'foundationYear' => $creationDate,
            'provinceId' => $province,
            'regionId' => $region,
            'townId' => $town,
            'districtId' => $district,
            'neighborhoodId' => $neighborhood,
            'openingHours' => $openingHours,
            'productTags' => $productTags,
            'address' => $address . ', ' . $number,
            'postalCode' => $postalCode,
            'email' => $email,
            'phone' => $phone,
            'web' => $web,
            'twitter' => $twitter,
            'facebook' => $facebook,
            'instagram' => $instagram,
            'pinterest' => $pinterest,
            'quitter' => $quitter,
            'description' => $description,
            'mainSectorId' => $mainSector,
            'sectorsIds' => $sectors,
            'legalFormId' => $legalForm,
            'socialEconomyNetworkIds' => $socialEconomyNetworks,
            'otherSocialEconomyNetworks' => $collaboratingWith,
            'socialBalance' => $socialBalance,
            'userConditionsAccepted' => $userConditionsAccepted,
            'newslettersAccepted' => $newslettersAccepted,
            'picture' => $picture,
            'polCoopLinks' => $polCoopTextUrls
            /*
             'polCoopNames' => $polCoopNames,
            'polCoopLinks' => $polCoopLinks
            */
        );

        $jsondata = json_encode(array_filter($data));
        $options = array (
            'http' => array (
                'header' => "Content-type: application/json",
                'method' => 'POST',
                'content' => $jsondata
            )
        );

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

    wp_redirect(home_url('/ok-intercooperacio'));
    exit();
    }
}

add_action("wp_loaded", "redirect_xinxeta_form");
function redirect_xinxeta_form (){
	if ( isset( $_POST["xinxeta-submit"])){

global $baseApiInternalUrl;
$username = isset( $_POST['username'] ) ? $_POST['username'] : '';
$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
$surname = isset( $_POST['surname'] ) ? $_POST['surname'] : '';
$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
$description = isset( $_POST['description'] ) ? $_POST['description'] : '';
$provinceId = isset( $_POST['provinceId'] ) ? $_POST['provinceId'] : '';
$regionId = isset( $_POST['regionId'] ) ? $_POST['regionId'] : '';
$townId = isset( $_POST['townId'] ) ? $_POST['townId'] : '';
$districtId = isset( $_POST['districtId'] ) ? $_POST['districtId'] : '';
$neighborhoodId = isset( $_POST['neighborhoodId'] ) ? $_POST['neighborhoodId'] : '';
$twitter = isset( $_POST['twitter'] ) ? $_POST['twitter'] : '';
$facebook = isset( $_POST['facebook'] ) ? $_POST['facebook'] : '';
$instagram = isset( $_POST['instagram'] ) ? $_POST['instagram'] : '';
$gitlab = isset( $_POST['gitlab'] ) ? $_POST['gitlab'] : '';
$fediverse = isset( $_POST['fediverse'] ) ? $_POST['fediverse'] : '';
$userConditionsAccepted = isset($_POST ['userConditionsAccepted']) ? $_POST ['userConditionsAccepted'] : null;
$newslettersAccepted = isset($_POST ['newslettersAccepted']) ? $_POST ['newslettersAccepted'] : null;

$url = $baseApiInternalUrl ."/addUser";
$data = array(
    'username' => $username,
    'name' => $name,
    'surname' => $surname,
    'email' => $email,
    'description' => $description,
    'provinceId' => $provinceId,
    'regionId' => $regionId,
    'townId' => $townId,
    'districtId' => $districtId,
    'neighborhoodId' => $neighborhoodId,
    'twitter' => $twitter,
    'facebook' => $facebook,
    'instagram' => $instagram,
    'gitlab' => $gitlab,
    'fediverse' => $fediverse,
    'userConditionsAccepted' => $userConditionsAccepted,
    'newslettersAccepted' => $newslettersAccepted,
    );
$jsondata = json_encode($data);
$options = array(
        'http' => array(
        'header' => "Content-type: application/json",
        'method' => 'POST',
        'content' => $jsondata
                )
            );

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);
wp_redirect(home_url('/ok-xinxeta'));
        exit();

}
}

add_action("wp_loaded", "redirect_butlleti_form");
function redirect_butlleti_form (){
	if ( isset( $_POST["butlleti-submit"])){

global $baseApiUrl;
$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
$email = isset( $_POST['email'] ) ? $_POST['email'] : '';

$url = $baseApiUrl ."/addNewsletterRegistration";
$data = array(
    'name' => $name,
    'email' => $email,
);
$jsondata = json_encode($data);
$options = array(
        'http' => array(
        'header' => "Content-type: application/json",
        'method' => 'POST',
        'content' => $jsondata
                )
            );

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);
wp_redirect(home_url('/ok-butlleti'));
    exit();

}
}

function directori_rewrite_tag() {
	add_rewrite_tag('%entityId%', '([^&]+)');
}
add_action('init', 'directori_rewrite_tag', 10, 0);

function directori_rewrite_rule() {
	add_rewrite_rule('directori/(.+)/?','index.php?page_id=83&entityId=$matches[1]','top');
	add_rewrite_rule('directori-intercoop/(.+)/?','index.php?page_id=4402&entityId=$matches[1]','top');
	add_rewrite_rule('(ca|es)/directori/(.+)/?','index.php?page_id=83&entityId=$matches[2]','top');
	add_rewrite_rule('(ca|es)/directori-intercoop/(.+)/?','index.php?page_id=4402&entityId=$matches[2]','top');
}

add_action('init', 'directori_rewrite_rule', 10, 0);

add_action('init', 'embedded_map_rewrite_rule', 10, 0);

function embedded_map_rewrite_rule() {
	$theme_name = next(explode('/themes/', get_stylesheet_directory()));

	global $wp_rewrite;
	$new_non_wp_rules = array(
			'map/embed(.*)' => 'wp-content/themes/' . $theme_name . '/map.php',
	);

	$wp_rewrite->non_wp_rules += $new_non_wp_rules;
}

// Bloqueig de les cookies de Google Analytics gestionat pel plugin EU Cookie Law
function blockGA(){
	echo '<script>';
  if ( function_exists('cookie_accepted') && cookie_accepted() ) {
	//cookies accepted
  } else {
	//cookies blocked
	echo "window['ga-disable-UA-57808927-1'] = true;";
  }
  echo '</script>';
}

/** If an url haven't https or http prefix, it adds https.
 *
 * @param string $link
 * @return mixed|string
 */
function convert_to_external_link($link) {
    if(!is_external_link($link)) {
        $link = "https://".$link;
    }
    return $link;
}

/** Checks if an url is http or https protocol.
 *
 * @param string $link
 * @return bool
 */
function is_external_link($link) {
    return substr($link, 0, 7 ) === "http://"
        or
        substr($link, 0, 7 ) === "https://";
}

/** Builds an object to backend TextUrlDto.
 *
 * @param $polCoopNames
 * @param $polCoopLinks
 * @return array of TextUrlDtos
 */
function build_pol_coop_text_urls($polCoopNames, $polCoopLinks) {

    $textsUrls[] = [];

    $index = 0;
    foreach ($polCoopNames as $name) {
        $textUrl = new stdClass();
        $textUrl->name = $name;
        $textUrl->url = $polCoopLinks[$index];

        $textsUrls[$index] = $textUrl;

        $index++;
    }

    return array_filter($textsUrls, function($value) { return !is_null($value);});
}

/** If remote image is empty, returns the path for default image. If not, returns the same url.
 * @param string $url
 * @param string $img
 * @param string $defaultImgPath
 * @return string
 */
function check_if_remote_image_is_empty(string $url, string $img, string $defaultImgPath) :string {
    list($width, $height, $type, $attr) = @getimagesize($url);
    if(empty($width)) {
        $img = $defaultImgPath;
    }
    return $img;
}

add_action('wp_head', 'blockGA', 0, 0);

wp_enqueue_script('plupload-all');

function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}

function get_excerpt($count){
    $excerpt = get_the_excerpt();
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.' ...';

    return $excerpt;
}

// Hook : to get content without images
function get_content_without_code($limit) {
    $content = explode(' ', get_the_content(), $limit);
    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }

    $content = preg_replace('`\[[^\]]*\]`', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
	$content = strip_tags($content);
	$content = substr($content, 0, 200);
	$content = substr($content, 0, strripos($content, " "));
	//$content = trim(preg_replace( '/s+/', ' ', $content));
	$content = $content.' ...';

    return $content;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);

    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }

    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);

    return $content;
}

add_filter( 'get_the_archive_title', function ($title) {
 if ( is_category() ) {
 $title = single_cat_title( '', false );
 } elseif ( is_tag() ) {
 $title = single_tag_title( '', false );
 } elseif ( is_author() ) {
 $title = '<span class="vcard">' . get_the_author() . '</span>' ;
 }

 return $title;
});

function dmc_add_svg_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'dmc_add_svg_mime_types');

/** Prints HTML tags with clickable badges for Xes Social Balance
 *  and Pam a Pam when necessary.
 *
 */
function build_balanc_and_pamapam_badges($entity) {
    $isExternal = $entity->entityStatus->entityStatusType == 'EXTERNAL';
    $status = $entity->entityStatus->statusType;
    $hasXesBalance = $entity->xesBalance;
    $isGreenCommerce = $entity->greenCommerceScope || $entity->greenCommerce;
    $visitaPerf = "";
    if (!$hasXesBalance && $isExternal && !$isGreenCommerce) {
        $visitaPerf = "<p> Aquesta iniciativa no esta a Pam a Pam <p>";
    } else if ($hasXesBalance && $isExternal) {
        $xesUrl = $entity->xesBalanceUrl ? $entity->xesBalanceUrl : "balanc-social";
        $visitaPerf = '<a href=\"'.$xesUrl.'" target="_blank"><img class="xes-bs-logo-center" src="/wp-content/themes/JointsWP-master/assets/images/b-social.png"></a>';
    } else if ($hasXesBalance && !$isExternal) {
        $xesUrl = $entity->xesBalanceUrl ? $entity->xesBalanceUrl : "balanc-social";
        $visitaPerf = '<a href="/directori/'.$entity->normalizedName.'" target="_blank"><img class="xes-bs-logo-center" src="/wp-content/themes/JointsWP-master/assets/images/xinxeta1.png"></a><a href="'.$xesUrl.'" target="_blank"><img class="xes-bs-logo-center" width="200px" src="/wp-content/themes/JointsWP-master/assets/images/b-social.png"></a>';
    } else if(!$hasXesBalance && !$isExternal){
        $visitaPerf = '<a href="/directori/'.$entity->normalizedName.'" target="_blank"><img class="xes-bs-logo-center" src="/wp-content/themes/JointsWP-master/assets/images/xinxeta1.png"></a>';
    }
    if($isGreenCommerce){
      $visitaPerf.= '<a href="/comerc-verd" target="_blank"><img class="xes-bs-logo-center" src="/wp-content/themes/JointsWP-master/assets/images/green-commerce-badge.png"></a>';
    }
    echo $visitaPerf;
}
