<?php
/*
Template Name: Economies comunitàries  (No Sidebar)
*/
?>

<?php get_header(); ?>
  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="col-xs-12 col-md-12">
              <div class="box-head">
                <div class="col-xs-12 col-md-12">
                 <h1 class="page-title"><?php the_title(); ?></h1>

                        <?php echo addVesAlMapa(); ?>
                      </div>
              </div>
            </div>

         <?php endif; ?>
      <iframe
              id="map-iframe"
              width="100%"
              height="600"
              frameborder="0" style="border:0" scrolling="no"
              src="https://pamapam.cat/map/embed?center=[41.8065428,0.9151375]&zoom=8&apiKey=7czthSTSdpcKOaf9yozubl1LhpSdWdoi"
              allowfullscreen>
      </iframe>

          <main id="main" class="container" role="main">
            <div class="col-xs-12 col-md-12">

			<!---- botons sota mapa ---->
		<div class="row">
			<div class="large-6 medium-6 small-12 columns">
					<a href="/ca/proposa-punt-economia-comunitaria/" class="button" style="width:-webkit-fill-available;margin: 5px 0;">
                  		PROPOSA UN NOU PUNT<span class="icon-arrow-right"></span>					
            		</a>
						<p>Si coneixes un projecte d’economia comunitària que no és al mapa</p>
					</div>
					<div class="large-6 medium-6 small-12 columns end">
					<a href="/ca/iniciativa-economia-comunitaria/" class="button" style="width:-webkit-fill-available;margin: 5px 0;">
                  		SUMA'T AL MAPA<span class="icon-arrow-right"></span>
            		</a>
						<p>Si ets una iniciativa d’economia comunitària i vols aparèixer al mapa</p>
					</div>
				</div>
				   
 			

		<!-- CUSTOM FIELDS -->
			

			<div class="row">
  				<div class="col-xs-12 col-lg-6  txt-destacado"><?php the_field('foto_text_esquerra'); ?></div>
 					 <div class="col-xs-12 col-lg-6 small-12 columns parrafo-foto"><?php the_field('foto_text_dreta'); ?></div>
			</div>
			

		</div>
			<div id="content">

		<div id="inner-content" class="row">

			<div class="col-xs-12 col-lg-12">
  				<h4 class="parrafo-g"><?php the_field('text_a_sota_de_la_foto'); ?></h4>
			</div>



		<div class="large-12 medium-12 small-12 columns back-green">
		<h4 class="txt-grey" style="text-align-last: center;"><?php the_field('titol_llista'); ?></h4>
			<div class="large-6 medium-6 small-12 columns col-izq">
 			 
  				<ul><?php the_field('llista'); ?></ul>
			</div>
		<div class="large-6 medium-6 small-12 columns col-dcha" style="font-weight:500;">
			 <?php the_field('text_a_sota_de_la_llista'); ?>
		</div>
    </div>


            </div>
          </main> 


		</div>


		<!---- Notícies d’economies comunitàries ---->
		<div class="container">
			<div class="row">
	    <main id="main" class="col-xs-12 col-lg-12 blog" role="main">
	    	<div class="title">
	    		<h4>Notícies d’economies comunitàries</h4>
	    	</div>
	    	<div class="noticies-intercoop row">
            <?php query_posts( array( 'category__and' => array(496), 'posts_per_page' => 3) );?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="col-xs-12 col-lg-4">
                		<?php get_template_part( 'parts/loop', 'archive' ); ?>
                	</div>
				<?php endwhile; ?>
		    <?php endif; wp_reset_query(); ?>
		  </div>



      	<!-- enllaç a veure més noticies -->
      
      	
					<nav class="page-navigation row">
						<div class="col-xs-12 col-md-6">
          		<a class="button" href="/ca/blog/category/economies-comunitaries">Veure més notícies d'economies comunitàries</a>
          	</div>
          	<div class="col-xs-12 col-md-6">
          		<a class="button" href="/ca/guia-per-a-la-identificacio-deconomies-comunitaries/">Guia d'identificació</a>
          	</div>
					</nav>

				</main> <!-- end #main -->
    	</div>
    </div>


    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->





<?php get_footer(); ?>


