<?php get_header(); ?>
	<div id="content">
		<div id="inner-content" class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="box-head">
						 <h1 class="page-title">Blog</h1>

		                <?php echo addVesAlMapa(); ?>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<main id="main" class="col-xs-12 col-lg-9" role="main">

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					    	<?php get_template_part( 'parts/loop', 'single' ); ?>

					    <?php endwhile; else : ?>

					   		<?php get_template_part( 'parts/content', 'missing' ); ?>

					    <?php endif; ?>

					</main> <!-- end #main -->
				<?php get_sidebar(); ?>
				</div>
			</div>

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>
