<footer class="footer" role="contentinfo">

	<div id="widgets">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-4">
					<?php dynamic_sidebar( 'pam_a_pam' ); ?>
				</div>
				<div class="col-xs-12 col-lg-4">
					<?php dynamic_sidebar( 'newsletter' ); ?>
				</div>
				<div class="col-xs-12 col-lg-4">
					<?php dynamic_sidebar( 'enlaces' ); ?>
				</div>
			</div>
		</div>
	</div> <!-- end #inner-footer -->

<div id="colabora">
	<div class="container">
		<div class="row">
			<div class="impulsa col-xs-12 col-lg-4">
				<p>Impulsa:</p>
				<a target="_blanc" href="https://xes.cat">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/xes.png" alt="Xarxa d'Economia Solidària " />
				</a>
			</div>
			<div class="suport col-xs-12 col-lg-8">
				<p>Amb el suport de:</p>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/aj_barcelona.png" alt="Pamapam" />
				</a>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/gene.png" alt="Pamapam" />
				</a>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/es.png" alt="Pamapam" />
				</a>
				<a href="#">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/sepe.png" alt="Pamapam" />
				</a>
			</div>
		</div>
	</div>
</div>

<div id="copyright-footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-lg-6 avis-footer">
				<strong><a href="/avis-legal/">Avís legal</a> - <a href="/politica-de-cookies/">Política de cookies</a> - <a href="/politica-de-privacitat/">Política de privacitat</a> </strong>
				<p>Disseny i desenvolupament web: Jamgo, Comuco i Dabne</p>
			</div>
			<div class="col-xs-12 col-lg-6 creativecommons">
				<p class="">
					<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" src="<?php echo get_template_directory_uri(); ?>/assets/images/cc.png" /></a>
				</p>
			</div>
		</div>
	</div>
</div>


</footer> <!-- end .footer -->




			</div>  <!-- end .main-content -->
		</div> <!-- end .off-canvas-wrapper -->


		<?php wp_footer(); ?>
	</body>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js" integrity="sha384-+sLIOodYLS7CIrQpBjl+C7nPvqq+FbNUBDunl/OZv93DB7Ln/533i8e/mZXLi/P+" crossorigin="anonymous"></script>

</html> <!-- end page -->
