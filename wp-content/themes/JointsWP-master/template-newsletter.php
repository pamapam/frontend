<?php /* Template Name: NEWSLETTER */ ?>


<?php get_header(); ?>
<!--
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>
-->
  <div id="content">

          <div id="inner-content" class="container-fluid">
            <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    <div class="box-head">
                       <h1 class="page-title"><?php the_title(); ?></h1>
                              <?php echo addVesAlMapa(); ?>
                    </div>
                  </div>
                </div>
              </div>

                  <div class="container">
                    <div id="cap-formulari" class="row">
                      <?php if(has_post_thumbnail()): ?>
                        <div class="col-xs-12 col-md-6">
                            <div class="img-formulari">
                                <?php the_post_thumbnail('full'); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <?php the_content(); ?>
                        </div>
                      <?php else : ?>
                        <div class="col-xs-12 col-md-12">
                            <?php the_content(); ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>


        <div class="container">
            <div class="row">
                <div class="col-xs-12 offset-lg-2 col-lg-8">

                <h3><?php
                                if (get_field('titol-butlleti')) {
                                    echo get_field('titol-butlleti');
                                }
                            ?></h3>

                <h4> <?php
                                if (get_field('texto-butlleti')) {
                                    echo get_field('texto-butlleti');
                                }
                            ?></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 offset-lg-2 col-lg-8">
                <form action="" class="xinxeta-form" method="post">
                    <div class="large-12 columns">
                        <br>
                        <p >El teu Nom</p>
                        <input type="text" name="name" class="input" required>
                    </div>
                    <div class="large-12 columns">
                        <p>El teu email</p>
                        <input type="mail" name="email" class="input" required>
                    </div>
                    <div class="large-12 columns">
                        <input type="checkbox" name="newslettersAccepted" id="newslettersAccepted" value="true" required>
                        <span>Accepto rebre notícies tal i com s’exposa <a href="/politica-de-privacitat/" target="_blank">aquí</a></span>
                        </input>
                    </div>
                    <div class="text-center columns">
                        <button type="submit" name="butlleti-submit" class="button-submit button" value="">
                            <span>Vull rebre l'actualitat de Pam a Pam</span>
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>


<script type="text/javascript">
    $(document).foundation();
</script>
</div>
</main>
</div>
</div>
<?php get_footer(); ?>




