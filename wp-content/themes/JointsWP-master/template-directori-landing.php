<?php
/*
 * Template Name: LANDING Directori
 */
?>
<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/sumoselect.css">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/jquery.sumoselect.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/directori-landing.js"></script>

<?php
function getSectors() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/mainSectors";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}
function getTerritories() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/territories";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}

$sectors = getSectors();
$territories = getTerritories()->response;
// Adjust the amount of rows in the grid
$grid_columns = 2;

$queryPost = array(
        'post_type'=> 'post',
        'post_status'=> 'publish',
        'posts_per_page'=> '2');

?>

<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />

<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
crossorigin=""></script>
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.Default.css">
<link rel="stylesheet" type="text/css" href="https://unpkg.com/leaflet.markercluster@1.1.0/dist/MarkerCluster.css">
<script src="https://unpkg.com/leaflet.markercluster@1.1.0/dist/leaflet.markercluster.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/js/leaflet-sidebar-master/L.Control.Sidebar.css" />
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/leaflet-sidebar-master/L.Control.Sidebar.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/home.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/map.js"></script>


<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseResourceUrl = \'' . $baseApiUrl . '\';';
echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';

if (isset($refererDomain)) {
	echo "var refererDomain = '" . $refererDomain . "'; ";
} else {
	echo 'var refererDomain = null; ';
}
if (isset($requestApiKey)) {
	echo "var apiKey = '" . $requestApiKey . "'; ";
} else {
	echo 'var apiKey = null; ';
}
if (isset($_GET['center'])) {
	echo 'var mapCenter = ' . $_GET['center'] . '; ';
} else {
	echo 'var mapCenter = [41.58, 1.84]; ';
}
if (isset($_GET['zoom'])) {
	echo 'var mapZoom = ' . $_GET['zoom'] . '; ';
} else {
	echo 'var mapZoom = 7.7; ';
}
?>
</script>
<div id="content">

	<div id="inner-content" class="container-fluid">

		<div class="box-head">
			<!--
			<h1 class="page-title"><?php the_title(); ?></h1>
	                <?php echo addVesAlMapa(); ?>
	            	-->
	                <p><?php the_content(); ?></p>
		</div>

		<main id="main" role="main" class="row">
			<div class="col-xs-12 col-md-8 col-lg-6 search-panel">

				<!-- Filtros y search. Mantener el form y las clases -->
				<div class="box-search">
					<form id="searchEntitiesForm2">
						<input type="submit" style="display: none" />
						<div class="row">
							<div class="large-6 columns">
								<select id="searchTerritory">
									<option id="0"></option><?php
									foreach($territories as $eachTerritory) {
		                            	$territoryId = $eachTerritory->id . '-' . $eachTerritory->type; ?>
		                            	<option id="<?php echo $territoryId; ?>" value="<?php echo $territoryId; ?>"><?php echo $eachTerritory->name; ?></option>
		                            <?php } ?>
								</select>
							</div>
							<div class="large-6 columns">
								<select id="searchSectors" multiple="multiple">
                                    <option></option>
                                    <?php foreach ($sectors as $eachSector) {?>
										<option id="<?php echo $eachSector->id; ?>" value="<?php echo $eachSector->id; ?>"><?php echo $eachSector->name; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="large-6 columns">
								<select id="searchSubsectors" multiple="multiple">
                                    <option></option>
								</select>
							</div>
							<div class="large-6 columns txt-search">
								<input id="searchText" type="text" name="text" value="" placeholder="Paraula clau">
								<div>
									<div id="resultats"></div>
								</div>
							</div>

							<div class="small-12 columns checkbox-search">
									<input id="greenCommerce" type="checkbox"/>
									<span>Mostrar punts Comerç Verd-Rezero</span>
							</div>
							<div class="large-6 columns buttons-search">
								<div id="reset">
									<button type="reset" class="control control-text">
										<span class="icon-iconos-my-roca-comparer"></span> Reiniciar la cerca
									</button>
								</div>
							</div>
							<div class="large-6 columns buttons-search">
								<div id="ok-search">
									<span class="search-filter">
										<img src="<?php echo get_template_directory_uri() . "/assets/images/fi-magnifying-glass.svg"; ?>"> Cerca
									</span>
								</div>
							</div>
						</div>
					</form>
				</div>
				<!-- Grilla de punts (establecimientos), no cambiar la clase -->
				<div id="punts" class="row"></div>
        <div class="loading">
          <div class="spinner"></div>
        </div>

      </div>
			<div class="col-xs-12 col-md-4 col-lg-6 search-map-container">
				<!-- MAPA Directori -->
				<?php
					include "map-body.php";
				?>
			</div>

		</main>
		<!-- end #main -->
	</div>
	<!-- end #inner-content -->
</div>
<!-- end #content -->
<script>
    <?php global $baseApiInternalUrl;?>

    $('#searchSectors').on('change', function() {
        let value = $(this).val();
        $.ajax({
                method: "GET",
                url: '<?php echo $baseApiInternalUrl?>' + "/sectors/?values=" + value
            }).done(function( response) {
                $('#searchSubsectors').empty();
                $('#searchSubsectors')[0].sumo.reload();
                response.forEach(function(obj) {
                    $('#searchSubsectors').append(`<option id="subsector-${obj.id}" data-parent="${obj.parent.id}" value="${obj.id}">${obj.name}</option>`);
                    $('#searchSubsectors')[0].sumo.add(obj.id,obj.name);
                });
            });
    });
</script>
<?php get_footer(); ?>
