<?php
/*
Template Name: LANDING Comunitat
*/
?>

<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/sumoselect.css">
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/sumoselect/jquery.sumoselect.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/comunitat-landing.js"></script>
<?php
function getCommunities() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/communities";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}
function getTerritories() {
	global $baseApiInternalUrl;
	$request = $baseApiInternalUrl . "/territories";
	$response = wp_remote_get($request);
	$json_response = json_decode(wp_remote_retrieve_body($response));
	return $json_response;
}

$communities = getCommunities()->response;
$territories = getTerritories()->response;
// Adjust the amount of rows in the grid
$grid_columns = 4;
?>

<div id="content">

    <div id="inner-content">
		<?php if(has_post_thumbnail()): ?>
		<div class="pagina-img-wrap">
		  <div class="pagina-title">
		    <h1><?php the_title(); ?></h1>
		  </div>
		  <?php the_post_thumbnail('full'); ?>
		</div>
		<?php else : ?>
		<div class="box-head">
		    <h1 class="page-title"><?php the_title(); ?></h1>
		    <?php echo addVesAlMapa(); ?>
		</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
	    		<main id="main" class="col-xs-12 col-md-12" role="main">
				<?php 
				if (get_field('descripcio_comunitat')) {
					echo get_field('descripcio_comunitat');
				}
				?>


			<div class="box-search">
				<form id="searchUsersForm">
					<input type="submit" style="display: none" />
					<div class="row">
						<div class="large-4 columns">
							<select id="searchTerritory">
								<option id="0"></option><?php
									foreach($territories as $eachTerritory) {
		                            	$territoryId = $eachTerritory->id . '-' . $eachTerritory->type; ?>
		                            	<option id="<?php echo $territoryId; ?>" value="<?php echo $territoryId; ?>"><?php echo $eachTerritory->name; ?></option>
		                            <?php } ?>
							</select>
						</div>
						<div class="large-4 columns">
							<select id="searchCommunity">
								<option id="0"></option><?php
									foreach ($communities as $eachCommunity) {?>
										<option id="<?php echo $eachCommunity->id; ?>" value="<?php echo $eachCommunity->id; ?>"><?php echo $eachCommunity->name; ?></option>
									<?php } ?>
							</select>
						</div>
						<div class="large-4 columns txt-search">
							<input id="searchName" type="text" name="name" value="" placeholder="Cerca per nom">
							<div>
								<div id="resultats"></div>
							</div>
						</div>

						<div class="large-12 columns">
							<div id="reset">
								<button type="reset" class="control control-text button">Tots<span class="icon-iconos-my-roca-comparer"></span>
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<!-- Grilla de voluntaris, no cambiar la clase -->
		    <div class="container">
				<div id="users" class="row">
				</div>
			</div>
		</main>
	</div>
</div>
	</div>
</div>

<!-- banner gran -->
<div class="contain-to-grid">
	<div class="large-12 columns comunitat banner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/fons-activista.jpg);">
		<div>
			<a href="<?php echo get_home_url();?>/xinxeta/">
				<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/icon/activista.svg' ); ?>
				<h3>Fes-te activista!</h3>
							<?php
				if (get_field('text_banner')) {
					echo get_field('text_banner');
				}
			?>
			</a>
		</div>
	</div>
</div>

<?php get_footer(); ?>
