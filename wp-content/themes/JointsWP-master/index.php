<?php /* Template Name: Inici No Sidebar */ ?>

<?php get_header(); ?>


<div id="content">

<?php
$my_query = new WP_Query('page_id=33');

while ($my_query->have_posts()) {
	$my_query->the_post();
	$do_not_duplicate = $post->ID;
}
?>
<?php if( have_rows('imatges_slider') ): ?>
	<?php while ( have_rows('imatges_slider') ) : the_row(); ?>
	<div id="slider-home" class="carousel slide float-left w-100 carousel-fade" data-ride="carousel">
		<ol class="carousel-indicators">
		<li data-target="#slider-home" data-slide-to="0" class="active"></li>
		<li data-target="#slider-home" data-slide-to="1"></li>
		<li data-target="#slider-home" data-slide-to="2"></li>
		<li data-target="#slider-home" data-slide-to="3"></li>
		</ol>
		<div class="carousel-inner">
		<div class="carousel-item active" data-mdb-interval="2000">
			<img class="d-block w-100" src="<?php the_sub_field('imatge_slide'); ?>" alt="<?php the_sub_field('titol_slide_1'); ?>">
			<div class="carousel-caption">
			<h5><?php the_sub_field('titol_slide_1'); ?></h5>
			<p class="d-none d-md-block"><?php the_sub_field('descripcio_slide_1'); ?></p>
			<a class="button d-md-block" href="<?php the_sub_field('enllac_slide_1'); ?>"><?php the_sub_field('boto_slide_1'); ?> <i class="fa fa-search" aria-hidden="true"></i></a>
			<p id="autor" class="d-none d-md-block"><?php the_sub_field('autor_imatge1'); ?></p>
			</div>
		</div>

		<div class="carousel-item" data-mdb-interval="2000">
			<img class="d-block w-100" src="<?php the_sub_field('imatge_slide_1'); ?>" alt="<?php the_sub_field('titol_slide_2'); ?>">
			<div class="carousel-caption">
			<h5><?php the_sub_field('titol_slide_2'); ?></h5>
			<p class="d-none d-md-block"><?php the_sub_field('descripcio_slide_2'); ?></p>
			<a class="button d-md-block" href="<?php the_sub_field('enllac_slide_2'); ?>"><?php the_sub_field('boto_slide_2'); ?> <i class="fa fa-search" aria-hidden="true"></i></a>
			<p id="autor" class="d-none d-md-block"><?php the_sub_field('autor_imatge2'); ?></p>
			</div>
		</div>

		<div class="carousel-item" data-mdb-interval="2000">
			<img class="d-block w-100" src="<?php the_sub_field('imatge_slide_2'); ?>" alt="<?php the_sub_field('titol_slide_3'); ?>">
			<div class="carousel-caption">
			<h5><?php the_sub_field('titol_slide_3'); ?></h5>
			<p class="d-none d-md-block"><?php the_sub_field('descripcio_slide_3'); ?></p>
			<a class="button d-md-block" href="<?php the_sub_field('enllac_slide_3'); ?>"><?php the_sub_field('boto_slide_3'); ?> <i class="fa fa-search" aria-hidden="true"></i></a>
			<p id="autor" class="d-none d-md-block"><?php the_sub_field('autor_imatge3'); ?></p>
			</div>
		</div>

		<div class="carousel-item" data-mdb-interval="2000">
			<img class="d-block w-100" src="<?php the_sub_field('imatge_slide_3'); ?>" alt="<?php the_sub_field('titol_slide_4'); ?>">
			<div class="carousel-caption">
			<h5><?php the_sub_field('titol_slide_4'); ?></h5>
			<p class="d-none d-md-block"><?php the_sub_field('descripcio_slide_4'); ?></p>
			<a class="button d-md-block" href="<?php the_sub_field('enllac_slide_4'); ?>"><?php the_sub_field('boto_slide_4'); ?> <i class="fa fa-search" aria-hidden="true"></i></a>
			<p id="autor" class="d-none d-md-block"><?php the_sub_field('autor_imatge4'); ?></p>
			</div>
		</div>

		</div>
		<a class="carousel-control-prev" href="#slider-home" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#slider-home" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
		</a>
	</div>
		<?php endwhile; ?>
	<?php endif; ?>


	<div id="info-home">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-4">
						<h3><?php if (get_field('titol_principal')) { echo get_field('titol_principal'); } ?></h3>
				</div>
				<div class="col-xs-12 col-lg-8"><p><?php if (get_field('descripcio_lateral')) { echo get_field('descripcio_lateral'); } ?><a href="<?php if (get_field('enllac_boto')) { echo get_field('enllac_boto'); } ?>"><?php if (get_field('enllac_descripcio')) { echo get_field('enllac_descripcio'); } ?></a></p>
				</div>
			</div>
		</div>
	</div>

		<div id="info-mapa">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-lg-12">
						<div class="info-mapa-img" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/fons-mapa.jpg);">
							<div class="fila fila1" onclick="location.href='/mapa';">
								<div class="text">
									<h5>Explora el mapa</h5>
									<p>Descobreix iniciatives al teu voltant, o arreu de Catalunya</p>
								</div>
								<div class="img">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/lupa-mapa.png" />
								</div>
							</div>
							<div class="fila fila2" onclick="location.href='/iniciativa';">
								<div class="text">
									<h5>Vols sortir al mapa?</h5>
									<p>Sol·licita una entrevista i si compleixes els criteris, formaràs part de l'engranatge.</p>
								</div>
								<div class="img">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/user-mapa.png" />
								</div>
							</div>
							<div class="fila fila3">
								<div class="text">
									<h5>Crea sinergies</h5>
									<p>Col·labora amb altres iniciatives d'ESS.</p>
								</div>
								<div class="img">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/sinergies-mapa.png" />
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<div class="container title">
            		<div class="row">
                		<div class="col-xs-12 col-md-12">
                    			<?php echo adrotate_group(1); ?>
                		</div>
            		</div>
        	</div>
		<div class="container title">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h4>Notícies</h4>
				</div>
			</div>
		</div>

		<div id="noticies" class="container">
			<div class="row">
				<div class="col-xs-12 col-lg-8">
					<div class="row">
	                <?php query_posts('posts_per_page=2');?>

	                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	                
	                	<div class="interior col-xs-12 col-md-6">
	                    	<?php get_template_part( 'parts/loop', 'home' ); ?>
	                	</div>
	            	
	                <?php endwhile; ?>
	                <?php endif; wp_reset_query(); ?>
	            </div>
				</div>

				<div class="banner col-xs-12 col-lg-4">
					<div class="row">
						<a href="/iniciativa/" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/fons_apareixer_mapa.jpg); margin-bottom: 40px;">
							<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/icon/xinxeta.svg' ); ?> <h6>Vols aparèixer<br>al mapa?</h6>
						</a>
					</div>
					<div class="row banner2">
						<a href="/activistes/" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/fons-activista.jpg);">
							<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/icon/activista.svg' ); ?>
							<h6>Fes-te<br>activista!</h6>
						</a>
					</div>
				</div>

			</div>
		</div>

		<div class="container title">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<h4>Eixos de Treball</h4>
				</div>
			</div>
		</div>

	<div id="eixos">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-lg-4 text-center">
					<a href="/agroecologia/">
						<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/icon/icon-agroecologia.svg' ); ?>
					<h5>Agroecologia</h5>
					</a>
				</div>
				<div class="col-md-12 col-lg-4 text-center">
					<a href="/intercooperacio/">
						<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/icon/intercooperacio_icon.svg' ); ?>
					<h5>Intercooperació</h5>
					</a>
				</div>
				<div class="col-md-12 col-lg-4 text-center">
					<a href="/economies-comunitaries/">
						<?php echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/icon/Ec_com_icon.svg' ); ?>
					<h5>Economies Comunitàries</h5>
					</a>
				</div>
			</div>
		</div>
	</div>

<?php get_footer(); ?>
