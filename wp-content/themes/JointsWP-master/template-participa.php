<?php /* Template Name: PARTICIPA */ ?>

<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="container">
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<div class="box-head container">
						 <h1 class="page-title">Participa</h1>
		                <?php echo addVesAlMapa(); ?>
					</div>
				</div>
			</div>

			<div class="row">
			    <main id="main" class="col-xs-12 col-md-12 participa" role="main">

				<!-- bloc 01 -->
					<div class="row out">

							<div class="large-6 small-12 columns">
								<a href="<?php echo get_site_url(); ?>/xinxeta/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/foto1.png" alt="Pamapam" /></a>
								<h3><a href="<?php echo get_site_url(); ?>/xinxeta/">Vols ser xinxeta?</a></h3>
							</div>
							<div class="large-6 small-12 columns">
								<p><a href="<?php echo get_site_url(); ?>/xinxeta/">Et formaràs en economia solidària, aniràs a entrevistar iniciatives i les pujaràs al mapa per a donar-les a conèixer!</a></p>
								<a href="<?php echo get_site_url(); ?>/xinxeta/"><span class="icon-arrow-right"></span></a>
							</div>
					</div> <!-- END bloc 01 -->

					<!-- bloc 02 -->
						<div class="row out">
								<div class="large-6 small-12 columns">
									<?php $upload_dir=wp_upload_dir();
	                                      $upload_dir=$upload_dir['baseurl'];
	                                ?>
	                                <a href="<?php echo get_site_url(); ?>/obrim-el-codi/"><img src="<?php echo $upload_dir; ?>/2019/02/comunitat_digital.png" alt="Pamapam" /></a>
									<h3><a href="<?php echo get_site_url(); ?>/obrim-el-codi/">Vols contribuir a millorar el nostre codi?</a></h3>
								</div>
								<div class="large-6 small-12 columns">
									<p><a href="<?php echo get_site_url(); ?>/obrim-el-codi/">Defensem el codi lliure i volem que serveixi per a que una comunitat àmplia el pugui utilitzar. Tant si vols contribuir-hi com a programadora com si vols crear-te una instància per a un altre projecte, tenim el codi obert per a tu!</a></p>
									<a href="<?php echo get_site_url(); ?>/obrim-el-codi/"><span class="icon-arrow-right"></span></a>
								</div>
						</div> <!-- END bloc 02 -->

						<!-- bloc 03 -->
						<div class="row out">
								<div class="large-6 small-12 columns">
									<a href="<?php echo get_site_url(); ?>/afegeix-una-iniciativa/"><img src="<?php echo $upload_dir; ?>/2019/02/afegeix_punt.png" alt="Pamapam" /></a>
									<h3><a href="<?php echo get_site_url(); ?>/afegeix-una-iniciativa/">Vols proposar un nou punt?</a></h3>
								</div>
								<div class="large-6 small-12 columns">
									<p><a href="<?php echo get_site_url(); ?>/afegeix-una-iniciativa/">Coneixes alternatives de consum responsable al teu barri? Ets una iniciativa d’economia solidària i t’agradaria aparèixer al web? Pots proposar noves iniciatives que anirem a entrevistar!</a></p>
									<a href="<?php echo get_site_url(); ?>/afegeix-una-iniciativa/"><span class="icon-arrow-right"></span></a>
								</div>
						</div> <!-- END bloc 03 -->



				</main> <!-- end #main -->


				<!-- Efectes caixes, TODO: incorporar-ho al js que es consideri -->
				    <script>
				    $('.row.out a img, .row.out h3 a, .row.out p a, .row.out span').hover(function() {
				  		var outWidget = $(this).parents('.row.out');
				  		$(outWidget).addClass("visualEffect");
				  	});
				  	$('.row.out a img, .row.out h3 a, .row.out p a, .row.out span').mouseout(function() {
				  		var outWidget2 = $(this).parents('.row.out');
				  		$(outWidget2).removeClass("visualEffect");
				  	});
				    </script>
			</div>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->


<?php get_footer(); ?>
