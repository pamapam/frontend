<?php /* Template Name: FES-TE XINXETA */ ?>


<?php

get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/xinxeta.js"></script>
<!--
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>
-->
<script>
    <?php
    echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
    echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
    ?>
</script>

<div id="content">

          <div id="inner-content" class="container-fluid">
            <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    <div class="box-head">
                       <h1 class="page-title"><?php the_title(); ?></h1>
                              <?php echo addVesAlMapa(); ?>
                    </div>
                  </div>
                </div>
              </div>

                  <div class="container">
                    <div id="cap-formulari" class="row">
                      <?php if(has_post_thumbnail()): ?>
                        <div class="col-xs-12 col-md-6">
                            <div class="img-formulari">
                                <?php the_post_thumbnail('full'); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <?php the_content(); ?>
                        </div>
                      <?php else : ?>
                        <div class="col-xs-12 col-md-12">
                            <?php the_content(); ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>


        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-12">
            <form action="" class="xinxeta-form" method="post">
                <div class="large-12 columns">
                    <p><strong>DADES BÀSIQUES</strong></p>
                </div>
                <div class="large-12 columns">
                    <p>Nom d'usuària*</p>
                    <input type="text" name="username" class="input" required>
                </div>
                <div class="large-12 columns">
                    <p>El teu Nom*</p>
                    <input type="text" name="name" class="input" required>
                </div>
                <div class="large-12 columns">
                    <p>Cognoms</p>
                    <input type="text" name="surname" class="input" required>
                </div>
                <div class="large-12 columns">
                    <p>El teu email*</p>
                    <input type="mail" name="email" class="input" required>
                </div>
                <div class="large-12 columns">
                    <p>Breu descripció* [què t'anima a formar part de la comunitat?]</p>
                    <textarea rows="5" cols="40" name="description" class="descripcio"></textarea>
                </div>
                <div class="large-12 columns">
                    <br>
                    <p><strong>ALTRES DADES</strong></p>
                </div>
                <div class="large-12 columns">
                    <p class="form-label">Província</p>
                    <select id="f_province" name="provinceId">
                    </select>
                </div>
                <div class="large-12 columns">
                    <p class="form-label">Comarca</p>
                    <select id="f_region" name="regionId">
                    </select>
                </div>
                <div class="large-12 columns">
                    <p class="form-label">Població</p>
                    <select id="f_town" name="townId">
                    </select>
                </div>
                <div class="large-12 columns">
                    <p class="form-label">Districte</p>
                    <select id="f_district" name="districtId">
                    </select>
                </div>
                <div class="large-12 columns">
                    <p class="form-label">Barri</p>
                    <select id="f_neighborhood" name="neighborhoodId">
                    </select>
                </div>

                <div class="large-12 columns">
                    <br>
                    <p><strong>XARXES SOCIALS</strong></p>
                </div>

                <div class="large-12 columns">
                    <p class="form-label">Twitter</p>
                    <input id="f_twitter" type="url" name="twitter">
                </div>

                <div class="large-12 columns">
                    <p class="form-label">Facebook</p>
                    <input id="f_facebook" type="url" name="facebook">
                </div>

                <div class="large-12 columns">
                    <p class="form-label">Instagram</p>
                    <input id="f_instagram" type="url" name="instagram">
                </div>

                <div class="large-12 columns">
                    <p class="form-label"> Gitlab</p>
                    <input id="f_gitlab" type="url" name="gitlab">
                </div>
                <div class="large-12 columns">
                    <p class="form-label">Fediverse</p>
                    <input id="f_fediverse" type="url" name="fediverse">
                </div>
                <div class="row">
                    <br>
                    <div class="large-12 columns">
                        <input type="checkbox" name="userConditionsAccepted" id="userConditionsAccepted" value="true" required>
                        <span>Accepto i he llegit les <a href="/avis-legal/" target="_blank">Condicions d’ús</a> i la <a href="/politica-de-privacitat/" target="_blank">Política de privacitat</a></span>
                        </input>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <br>
                        <input type="checkbox" name="newslettersAccepted" id="newslettersAccepted" value="true">
                        <span>Accepto rebre butlletins, segons s’especifica <a href="/politica-de-privacitat/" target="_blank">aquí</a></span>
                        </input>
                    </div>
                </div>

                <div class="small-8 columns">
                    <button type="submit" name="xinxeta-submit" class="button-submit button" value="">
                        <span>Vull fer-me activista</span>
                    </button>
                </div>

            </form>

            <div class="small-12 small-centered columns">
                <h5>
                    <?php
                    if (get_field('politica-dades')) {
                        echo get_field('politica-dades');
                    }
                    ?>
                </h5>
            </div>
            </div>
        </div>
    </div>


</div>
</div>
<script type="text/javascript">
    $(document).foundation();
</script>
<?php get_footer(); ?>
