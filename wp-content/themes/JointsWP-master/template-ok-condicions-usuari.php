<?php /* Template Name: GRACIES-Condicions Usuari */ ?>

<?php get_header(); ?>

  <div id="content">
          <div id="inner-content" class="container-fluid">
      <div class="container">
        <div class="row">
          <main id="main" class="ok col-xs-12 col-lg-12" role="main">
                <h4><?php
                if (get_field('texto-ok-condicions-usuari')) {
                echo get_field('texto-ok-condicions-usuari');
                }
                ?></h4>
                <?php //the_content(); ?>

                <img src="<?php echo get_template_directory_uri()?>/assets/images/pamapam-logo-bn.png" />

                <a href="<?php echo get_site_url();?>" class="button button-large">Tornar a la pàgina principal</a>
            </div>
          </main>
        </div>
      </div>

    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->

<?php get_footer(); ?>