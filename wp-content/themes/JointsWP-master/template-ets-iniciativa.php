<?php /* Template Name: ETS-INICIATIVA */ ?>


<?php

get_header();
?>
<script src="<?php echo get_template_directory_uri(); ?>/js/ets-iniciativa.js"></script>
<!--
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>
-->
<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
$scope = get_field('ambit-iniciativa');
$externalFilterTags = get_field('filtres-mapeig');
?>
</script>



<div id="content">

          <div id="inner-content">
          	<div class="container">
	            <div class="row">
	              <div class="col-xs-12 col-md-12">
	                <div class="box-head">
	                   <h1 class="page-title"><?php the_title(); ?></h1>
	                          <?php echo addVesAlMapa(); ?>
	                </div>
	              </div>
	            </div>
	          </div>

			      <div class="container">
			        <div id="cap-formulari" class="row">
			          <?php if(has_post_thumbnail()): ?>
			            <div class="col-xs-12 col-md-6">
			                <div class="img-formulari">
			                    <?php the_post_thumbnail('full'); ?>
			                </div>
			            </div>
			            <div class="col-xs-12 col-md-6">
			              <?php the_content(); ?>
			            </div>
			          <?php else : ?>
			            <div class="col-xs-12 col-md-12">
			                <?php the_content(); ?>
			            </div>
			          <?php endif; ?>
			        </div>
			      </div>


        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-12">
				<form action= get_site_url() . "ok-ets-iniciativa/" . "add&noheader=true" class="xinxeta-form" method="post">
					<!--DADES BASISQUES-->
					<div class="row">
						<div class="large-12 columns">
							<p><strong>DADES BÀSIQUES</strong></p>
							<input id="f_scope" type="hidden" name="f_scope" value="<?php echo $scope; ?>" />
							<input id="f_externalFilterTags" type="hidden" name="f_externalFilterTags" value="<?php echo $externalFilterTags; ?>" />
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Nom de la iniciativa</p>
							<input id="f_name" type="text" name="f_name" class="input" required>
						</div>
						<div class="large-6 columns">
							<p class="form-label">NIF/CIF</p>
							<input id="f_nif" type="text" name="f_nif" class="input">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Província</p>
							<select id="f_province" name="f_province">
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Comarca</p>
							<select id="f_region" name="f_region">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Població</p>
							<select id="f_town" name="f_town">
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Districte</p>
							<select id="f_district" name="f_district">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Barri</p>
							<select id="f_neighborhood" name="f_neighborhood">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Horari</p>
							<input id="f_openingHours" type="text" name="f_openingHours">
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Què hi pots trobar?</p>
							<input type="text" id="f_productTags" name="f_productTags" placeholder="<?php esc_attr_e ('Etiqueta1 Etiqueta2 Etiqueta3 (sense commas)');?>">
						</div>
					</div>
					<!--ADREÇA-->
					<div class="row">
						<div class="large-12 columns">
							<br>
							<p><strong>ADREÇA</strong></p>
						</div>
					</div>
					<div class="row">
						<div class="large-8 columns">
							<p class="form-label">Avinguda, carrer, plaça...</p>
							<input id="f_address" type="text" name="f_address" class="input" placeholder="<?php esc_attr_e ('Adreça');?>" required>
						</div>
						<div class="large-4 columns">
							<p class="form-label">Número</p>
							<input id="f_number" type="text" name="f_number" class="input" required>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Correu elèctronic de contacte</p>
							<input id="f_email" type="mail" name="f_email" class="input" required pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$">
						</div>
						<div class="large-6 columns">
							<p class="form-label">Telèfon de contacte</p>
							<input id="f_phone" type="tel" name="f_phone">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Lloc web</p>
							<input id="f_web" type="url" name="f_web">
						</div>
						<div class="large-6 columns">
							<p class="form-label">Twitter</p>
							<input id="f_twitter" type="url" name="f_twitter">
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Facebook</p>
							<input id="f_facebook" type="url" name="f_facebook">
						</div>
						<div class="large-6 columns">
							<p class="form-label">Instagram</p>
							<input id="f_instagram" type="url" name="f_instagram">
						</div>
					</div>
					<div class="row">
					    <div class="large-12 columns">
                            <p class="form-label">Any de fundació de la iniciativa</p>
                            <input id="f_fundacio" type="number" name="f_fundacio">
                        </div>
                    </div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Breu descripció de la iniciativa</p>
                            <textarea id="f_description" name="f_description" cols="40" rows="5" class="descripcio"></textarea>
						</div>
					</div>
					<!--AMBIT DE L'ECONOMIA SOCIAL I SOLIDARIA-->
					<div class="row">
						<div class="large-12 columns">
							<br>
							<p><strong>ÀMBIT DE L'ECONOMIA SOCIAL I SOLIDÀRIA</strong></p>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Sectors</p>
							<select id="f_mainSector" name="f_mainSector">
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Forma jurídica</p>
							<select id="f_legalForm" name="f_legalForm">
							</select>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<p class="form-label">Xarxes a les que es pertany</p>
							<select id="f_socialEconomyNetworks" name="f_socialEconomyNetworks[]" multiple>
							</select>
						</div>
						<div class="large-6 columns">
							<p class="form-label">Iniciatives amb les que es col.labora</p>
							<textarea id="f_collaboratingWith" name="f_collaboratingWith" cols="40" rows="5" class="descripcio"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<p class="form-label">Balanç social</p>
							<div class="radioButton">
								<input type="radio" name="socialBalance" value="1"><label>Sí</label>
								<input type="radio" name="socialBalance" value="2" checked><label>No</label>
								<input type="radio" name="socialBalance" value="3"><label>No aplica</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
						</div>
					</div>
					<div class="row">
						<br>
						<div class="large-12 columns">
							<input type="checkbox" name="userConditionsAccepted" id="userConditionsAccepted" value="true" required>
								<span>Accepto i he llegit les <a href="/avis-legal/" target="_blank">Condicions d’ús</a> i la <a href="/politica-de-privacitat/" target="_blank">Política de privacitat</a>.</span>
							</input>
						</div>
					</div>
					<div class="row">
						<div class="large-12 columns">
							<input type="checkbox" name="newslettersAccepted" id="newslettersAccepted" value="true" required>
								<span>Accepto rebre notícies tal i com s’exposa <a href="/politica-de-privacitat/" target="_blank">aquí</a>.</span>
							</input>
						</div>
					</div>
					<div class="row">
						<div class="large-6 columns">
							<button type="submit" name="iniciativa-submit" class="button-submit button" value="">
								<span>Proposa una iniciativa</span>
							</button>
						</div>
					</div>
				</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).foundation();
</script>

<?php get_footer(); ?>
