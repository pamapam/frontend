<?php /* Template Name: Pàgina Full amb Capçalera */ ?>

<?php get_header(); ?>

  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-md-12">
                  <div class="box-head">
                     <h1 class="page-title"><?php the_title(); ?></h1>

                            <?php echo addVesAlMapa(); ?>
                  </div>
                </div>
              </div>
            </div>

         <?php endif; ?>
      <div class="contingut">
          <main id="main" role="main">
              <?php the_content(); ?>
          </main>
      </div>

    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->

<?php get_footer(); ?>