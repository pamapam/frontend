<!-- inicio mapa -->
<!-- no cambiar el id. Requiere el plugin leaflet-sidebar-master -->
<div id="sidebar">
    <p></p>
</div>
<!-- inicio lista filtros. Requiere mapbox. No cambiar id -->
<!-- fin lista filtros -->
<!-- overlay para el movil-->
<div class="overlay " onClick="style.pointerEvents='none'"></div>

<div id="map" class="contain-to-grid">
    <div id="geolocalitzacio" class="buttons-on-map geolocalitzacio">
        <a href="#" class="button"><span class="icon-ic_room_48px"></span>GEOLOCALITZA'M</a>
    </div>
    <?php if ($requestApiKey != null): ?>
        <div id="add-iniciativa" class="buttons-on-map add-iniciativa">
			<a href="https://pamapam.cat/"  target="_blank" class="">
				<img  src="<?php echo get_template_directory_uri() . "/assets/images/pamapam-logo-bn.png"; ?>" style="width: 150px;">
			</a>
		</div>
    <?php endif; ?>
    <div class="search-box-panel">
        <form method="post" id="searchEntitiesForm" action="<?php echo $baseApiUrl . "/searchEntitiesGeojson"; ?>">
        </form>
    </div>
</div>

<div id='searchContainer' style="position: fixed;"></div>
