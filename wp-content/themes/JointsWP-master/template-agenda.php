<?php /* Template Name: AGENDA */ ?>

<?php get_header(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/activitats-landing.js"></script>
  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="col-xs-12 col-md-12">
              <div class="box-head">
                <div class="col-xs-12 col-md-12">
                 <h1 class="page-title"><?php the_title(); ?></h1>

                        <?php echo addVesAlMapa(); ?>
                      </div>
              </div>
            </div>

         <?php endif; ?>

<?php

 $today = current_time('Y-m-d');

 $queryPost = array( 'post_type'=> 'Agenda',
  'post_status'=> 'publish',
  'meta_key'      => 'data',
  // només les activitats futures
  'meta_query'             => array(
      array(
          'key'       => 'data',
          'value'     => $today,
          'compare'   => '>=',
          'type'      => 'DATE',
      ),
  ),
  'orderby'     => 'meta_value',
  'order'       => 'ASC'
     //'posts_per_page'=> '10',
     //'paged' => get_query_var('paged')
   )
?>

<!-- fin loop de consulta a la base de dades  marga-- >



      <!-- inici llista -->

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-md-12">

      <?php query_posts($queryPost);?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php get_template_part( 'parts/loop', 'agenda' ); ?>
      <?php endwhile; ?>
      <?php endif; wp_reset_query(); ?>

    </div>
  </div>
</div>
      <!-- fi llista -->

  </div>
</div>







<!-- inici footer eric-->

<?php get_footer(); ?>

<!-- fi footer -->
