<?php
/*
Template Name: Reculls Pràctics
*/
?>

<?php get_header(); ?>
  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="col-xs-12 col-md-12">
              <div class="box-head">
                <div class="col-xs-12 col-md-12">
                 <h1 class="page-title"><?php the_title(); ?></h1>

                        <?php echo addVesAlMapa(); ?>
                      </div>
              </div>
            </div>

         <?php endif; ?>
      <div class="container">
        <div class="row">
          <main id="main" class="col-xs-12 col-md-12" role="main">
              <?php the_content(); ?>
          </main>
        </div>
      </div>



		<!---- Notícies Reculls pràctics ---->
		<div class="container">
			<div class="row">
	    <main id="main" class="col-xs-12 col-md-12 blog" role="main">
	    	<div class="title">
	    		<h4>Propostes de consum responsable pel dia a dia</h4>
	    	</div>
	    	<div id="reculls" class="noticies-intercoop row">
            <?php $query = new WP_Query( array( 'tag' => 'reculls-practics', 'posts_per_page' => 15) );?>
				<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
					<div class="col-xs-12 col-lg-4">
                		<?php get_template_part( 'parts/loop', 'archive-reculls' ); ?>
                	</div>
				<?php endwhile;?>
                <div class="col-xs-12 col-md-12">
                <?php joints_page_navi_reculls(); ?>
                </div>
        <?php endif; wp_reset_query(); ?>
		  </div>


				</main> <!-- end #main -->
    	</div>
    </div>


    </div> <!-- end #inner-content -->
  </div> <!-- end #content -->


<?php get_footer(); ?>


