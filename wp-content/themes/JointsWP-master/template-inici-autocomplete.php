<?php
/*
Template Name: Inici autocomplete (No Sidebar)
*/
?>
<!-- https://github.com/utahemre/Leaflet.GeoJSONAutocomplete -->
<?php get_header(); ?>
			
	<div id="content">
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">

<!-- inicio mapa -->
    <!-- esto a hoja de estilos -->
    <style type="text/css">
    #map {
        width: 100%;
        height: 400px;
    }
    </style>

<div id="map"></div>
<div id='searchContainer' style="position: fixed;">

    <script type="text/javascript">


        var xarxes = L.icon({
            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/xar.png',
            /*
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            shadowUrl: 'http://pamapam.cat/sites/all/themes/pamapam/images/map/xar.png',
            shadowSize: [41, 41],
            shadowAnchor: [13, 41],
            popupAnchor:  [0, -28]
            */
        });

        var vestits = L.icon({
            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/ves.png',
            /*
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            shadowUrl: 'http://pamapam.cat/sites/all/themes/pamapam/images/map/xar.png',
            shadowSize: [41, 41],
            shadowAnchor: [13, 41],
            popupAnchor:  [0, -28]
            */
        });

        var alimentacio = L.icon({
            iconUrl: '<?php echo get_template_directory_uri(); ?>/icones/ali.png',
            /*
            iconSize: [25, 41],
            iconAnchor: [12, 41],
            shadowUrl: 'http://pamapam.cat/sites/all/themes/pamapam/images/map/xar.png',
            shadowSize: [41, 41],
            shadowAnchor: [13, 41],
            popupAnchor:  [0, -28]
            */
        });

        var mapboxTiles = L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '<a href="http://www.mapbox.com/about/maps/" target="_blank">Terms &amp; Feedback</a>'
        });

        var map = L.map('map')
            .addLayer(mapboxTiles)
            .setView([39.80, 34.00], 6); /* centrado en Turquía */

            // Add layer for markercluster
            var markers = L.markerClusterGroup();

            // input filtros
            var options = {
                geojsonServiceAddress: "<?php echo get_template_directory_uri(); ?>/testgeojsondata.json"
                };
                $("#searchContainer").GeoJsonAutocomplete(options);



    </script>



<!-- fin mapa -->				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
