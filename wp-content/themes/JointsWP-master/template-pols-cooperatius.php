<?php
/*
Template Name: Pols Cooperatius  (No Sidebar)
*/
?>

<?php get_header(); ?>
  <div id="content">
        <?php if(has_post_thumbnail()): ?>
          <div id="inner-content">
            <div class="pagina-img-wrap">
              <div class="pagina-title">
                <h1><?php the_title(); ?></h1>
              </div>
              <?php the_post_thumbnail('full'); ?>
            </div>
        <?php else : ?>
          <div id="inner-content" class="container-fluid">
            <div class="col-xs-12 col-md-12">
              <div class="box-head">
                <div class="col-xs-12 col-md-12">
                 <h1 class="page-title"><?php the_title(); ?></h1>

                        <?php echo addVesAlMapa(); ?>
                      </div>
              </div>
            </div>

         <?php endif; ?>

          <main id="main" class="container" role="main">
            <div class="col-xs-12 col-md-12">
              <?php the_content(); ?>
            </div>
          </main>

              <iframe
                      id="map-iframe"
                      width="100%"
                      height="600"
                      frameborder="0" style="border:0" scrolling="no"
                      src="https://pamapam.cat/map/embed?center=[41.7,1.04]&zoom=8&apiKey=1M6c47h53a2dQLRMiam9bELV93LS1lpr"
                      allowfullscreen>
              </iframe>

		<!---- botons sota mapa ---->
		<?php		
		$today = current_time('Y-m-d');

$queryPost = array( 'post_type'=> 'agenda_pols_cooperat',
 'post_status'=> 'publish',
 'meta_key'			=> 'data',
 // només les activitats futures - això és comenta per que ara les volen totes
 /* 'meta_query'             => array(
	 array(
		 'key'       => 'data',
		 'value'     => $today,
		 'compare'   => '>=',
		 'type'      => 'DATE',
	 ),
 ),*/
 'orderby'			=> 'meta_value',
   'order'				=> 'DES'
	//'posts_per_page'=> '10',
	//'paged' => get_query_var('paged')
  )
?>

<div class="container">
  <div class="row">
    <div class="col-xs-12 col-md-12">
      <?php query_posts($queryPost);?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php get_template_part( 'parts/loop', 'agenda' ); ?>
      <?php endwhile; ?>
      <?php endif; wp_reset_query(); ?>
    </div>
  </div>
</div>
   
<!-- lista de recursos -->

<?php

$queryPost = array( 'post_type'=> 'recurs_pol_cooperati',
 'post_status'=> 'publish',
 'meta_key'     => 'data',
 
 'orderby'      => 'meta_value',
   'order'        => 'ASC'
  )

?>
<!-- requadre -->
<div class="container" style="border: 2px solid #F49339; margin-top: 24px; margin-bottom: 24px; padding-top: 12px;">
  <div class="row">
    <div class="col-xs-12 col-md-12">

      <h2 class="txt-grey" style="text-align-last: center;">Recursos</h2>

      <ul>

      <?php

      ?>
      <?php query_posts($queryPost);?>
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
         <li><a target="_blank" href="<?php the_field('link_pdf', $post->ID); ?>"><?php the_title(); ?></a>
         <?php // la data, no la volen
           // $originalDate =  get_post_meta($post->ID, 'data', true);
           // $newDate = date("d/m/Y", strtotime($originalDate));
           // echo $newDate;
         ?>
         <?php the_content(); ?>
         </li>
      <?php endwhile; ?>
      <?php endif; wp_reset_query(); ?>

      </ul>
    </div>
  </div>
</div> <!-- requadre -->

<!-- fi llista de recursos -->

</div>
</div>

<?php get_footer(); ?>


