<?php /* Template Name: FES-TE VOLUNTARIA */ ?>


<?php
get_header(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/voluntaria.js"></script>
<!--
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.min.js"></script>
-->
<script>
<?php
echo 'var jsSiteUrl = \'' . get_site_url() . '\'; ';
echo 'var jsBaseApiUrl = \'' . $baseApiUrl . '\'; ';
$scope = get_field('ambit-iniciativa');
$externalFilterTags = get_field('filtres-mapeig');
?>
</script>


<div id="content">

          <div id="inner-content">
            <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-md-12">
                    <div class="box-head">
                       <h1 class="page-title"><?php the_title(); ?></h1>
                              <?php echo addVesAlMapa(); ?>
                    </div>
                  </div>
                </div>
            </div>

                  <div class="container">
                    <div id="cap-formulari" class="row">
                      <?php if(has_post_thumbnail()): ?>
                        <div class="col-xs-12 col-md-6">
                            <div class="img-formulari">
                                <?php the_post_thumbnail('full'); ?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <?php the_content(); ?>
                        </div>
                      <?php else : ?>
                        <div class="col-xs-12 col-md-12">
                            <?php the_content(); ?>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>


        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-12">
                    <form action="" class="xinxeta-form" method="POST">
                        <!--DADES BASISQUES-->
                        <div class="row">
                            <div class="large-12 columns">
                                <p><strong>DADES BÀSIQUES</strong></p>
    							<input id="f_scope" type="hidden" name="f_scope" value="<?php echo $scope; ?>" />
    							<input id="f_externalFilterTags" type="hidden" name="f_externalFilterTags" value="<?php echo $externalFilterTags; ?>" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="large-6 columns">
                                <p class="form-label">Nom de la iniciativa</p>
                                <input id="f_name" type="text" name="f_name" class="input" required>
                            </div>
                            <div class="large-6 columns">
                                <p class="form-label">Província</p>
                                <select id="f_provinces" name="f_province">
                                </select>
                            </div>
                        </div>
                        <div class="row">
                        <div class="large-6 columns">
                                <p class="form-label">Comarca</p>
                                <select id="f_regions" name="f_region">
                                </select>
                        </div>

                        <div class="large-6 columns">
                                <p class="form-label">Població</p>
                                <select id="f_towns" name="f_town">
                                </select>
                        </div>
                        </div>

                        <div class="row">

                           <div class="large-6 columns">
                                <p class="form-label">Districte</p>
                                <select id="f_districts" name="f_district">
                                </select>
    	                   </div>

                           <div class="large-6 columns">
                                <p class="form-label">Barri</p>
                                <select id="f_neighborhoods" name="f_neighborhood">
                                </select>
                           </div>
                        </div>
                        <div class="row">
                            <div class="large-8 columns">
                                <p class="form-label">Avinguda, carrer, plaça...</p>
                                <input id="f_address" type="text" name="f_address" class="input">
                            </div>
                            <div class="large-4 columns">
                                <p class="form-label">Número</p>
                                <input id="f_number" type="text" name="f_number" class="input">
                            </div>
                        </div>
    					<div class="row">
    						<div class="large-6 columns">
    							<p class="form-label">Correu elèctronic de contacte</p>
    							<input id="f_email" type="mail" name="f_email" class="input" pattern="^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$">
    						</div>
    						<div class="large-6 columns">
    							<p class="form-label">Telèfon de contacte</p>
    							<input id="f_phone" type="tel" name="f_phone">
    						</div>
    					</div>
    					<div class="row">
    						<div class="large-6 columns">
    							<p class="form-label">Lloc web</p>
    							<input id="f_web" type="url" name="f_web">
    						</div>
    					</div>
                        <div class="row">
                            <div class="large-12 columns">
                                <p class="form-label">Breu descripció de la iniciativa</p>
                                <textarea id="f_description" name="f_description" cols="40" rows="15" class="descripcio"></textarea>
                            </div>
                        </div>
                        <div class="row">
                        <div class="large-6 columns">
                            <button type="submit" name="voluntaria-submit" class="button-submit button" value="">
                                <span>Proposa una iniciativa</span>
                            </button>
                        </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).foundation();
</script>

<?php get_footer(); ?>
